var fs = require('fs');

fs.stat("./rom.gb", function (err, stats) {
    const fileSize = stats.size;
    const bytesToRead = fileSize;
    const position = fileSize - bytesToRead;
    fs.open("./rom.gb", 'r', function (errOpen, fd) {
        fs.read(fd, Buffer.alloc(bytesToRead), 0, bytesToRead, position, function (errRead, bytesRead, buffer) {
            const bytes = [];
            for (const byte of buffer.values()) {
                bytes.push(byte);
            }

            fs.writeFileSync("./out.js", "export const rom = " + JSON.stringify(bytes));
        });
    });
});