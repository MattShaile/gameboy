import { GPU } from "gpu";
import { Input } from "input";
import { MapViewer } from "map-viewer";
import { Memory } from "memory";
import { Registers } from "registers";
import { Screen } from "screen";
import { APU } from "sound/apu";
import { TileViewer } from "tile-viewer";
import { Timer } from "timer";
import { Z80 } from "z80";

const memory = new Memory();
const registers = new Registers(memory);
const timer = new Timer(memory, registers);

const screen = new Screen(memory, registers);

const map = new MapViewer(memory, registers);
const tiles = new TileViewer(memory, registers);

const apu = new APU(memory, registers);
const gpu = new GPU(memory, registers, screen);
const input = new Input(memory);
const cpu = new Z80(memory, registers, gpu, timer, input);

const stopButton = document.getElementById("stop") as HTMLInputElement;
const breakButton = document.getElementById("break") as HTMLButtonElement;
const stepButton = document.getElementById("step") as HTMLButtonElement;
const resetButton = document.getElementById("reset") as HTMLButtonElement;
const gameSelect = document.getElementById("game") as HTMLButtonElement;

(window as any).memory = memory;
(window as any).reg = registers;
(window as any).logs = false;

function tick() {
    while (!gpu.vblank) {
        if (!stopButton.checked) {
            break;
        }

        cpu.tick();
    }

    gpu.vblank = false;

    tiles.draw();
    registers.draw();
    map.drawBG();

    requestAnimationFrame(() => tick());
}

stepButton.onclick = () => {

    Z80.debug = true;

    cpu.tick();

    tiles.draw();
    registers.draw();
    map.drawBG();

    Z80.debug = false;
};

breakButton.onclick = () => {
    stopButton.checked = false;
    Z80.debug = true;
}

resetButton.onclick = () => {
    registers.reset();
    memory.reset();
}

gameSelect.onchange = () => {
    registers.reset();
    memory.reset();
}

requestAnimationFrame(() => tick());

// const my = mine.split(",");
// const yours = theirs.split(",");

// my.forEach((a:string, index:number) => {
//     const b = yours[index];

//     if (a !== b) {
//         console.log(a, b, index);
//         debugger;
//     }
// })
