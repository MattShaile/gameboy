import { ALU, ALUOperation } from "instructions/alu";
import { HALT } from "instructions/halt";
import { Instruction } from "instructions/instruction";
import { Interrupt, InterruptType } from "instructions/interrupt";
import { JP, JumpCondition, JumpType } from "instructions/jp";
import { LD, LoadTarget } from "instructions/ld";
import { NOP } from "instructions/nop";
import { Stack, StackType } from "instructions/stack";
import { STOP } from "instructions/stop";
import { Memory } from "memory";
import { Registers } from "registers";
import { Z80 } from "z80";

export class Decoder {
    private instructionMap: Instruction[];
    private instructionMapCB: Instruction[];

    constructor(
        protected z80: Z80,
        protected memory: Memory,
        protected registers: Registers
    ) {
        this.instructionMap = [];
        this.instructionMapCB = [];

        // 0x00 - 0x0F
        this.instructionMap.push(new NOP(4, 1));
        this.instructionMap.push(new LD(LoadTarget.BC, LoadTarget.U16, 12, 3));
        this.instructionMap.push(new LD(LoadTarget.M_BC, LoadTarget.A, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.INC, LoadTarget.BC, LoadTarget.BC, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.INC, LoadTarget.B, LoadTarget.B, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.DEC, LoadTarget.B, LoadTarget.B, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.B, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.RLCA, LoadTarget.A, LoadTarget.A, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.M_U16, LoadTarget.SP, 20, 5));
        this.instructionMap.push(new ALU(ALUOperation.ADD, LoadTarget.HL, LoadTarget.BC, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.M_BC, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.DEC, LoadTarget.BC, LoadTarget.BC, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.INC, LoadTarget.C, LoadTarget.C, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.DEC, LoadTarget.C, LoadTarget.C, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.C, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.RRCA, LoadTarget.A, LoadTarget.A, 4, 1));

        // 0x10 - 0x1F
        this.instructionMap.push(new STOP(4, 1));
        this.instructionMap.push(new LD(LoadTarget.DE, LoadTarget.U16, 12, 3));
        this.instructionMap.push(new LD(LoadTarget.M_DE, LoadTarget.A, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.INC, LoadTarget.DE, LoadTarget.DE, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.INC, LoadTarget.D, LoadTarget.D, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.DEC, LoadTarget.D, LoadTarget.D, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.D, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.RLA, LoadTarget.A, LoadTarget.A, 4, 1));
        this.instructionMap.push(new JP(JumpType.RELATIVE, JumpCondition.ALWAYS, LoadTarget.U8, 12, 3, 12, 3));
        this.instructionMap.push(new ALU(ALUOperation.ADD, LoadTarget.HL, LoadTarget.DE, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.M_DE, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.DEC, LoadTarget.DE, LoadTarget.DE, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.INC, LoadTarget.E, LoadTarget.E, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.DEC, LoadTarget.E, LoadTarget.E, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.E, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.RRA, LoadTarget.A, LoadTarget.A, 4, 1));

        // 0x20 - 0x2F
        this.instructionMap.push(new JP(JumpType.RELATIVE, JumpCondition.NZ, LoadTarget.U8, 8, 2, 12, 3));
        this.instructionMap.push(new LD(LoadTarget.HL, LoadTarget.U16, 12, 3));
        this.instructionMap.push(new LD(LoadTarget.M_HL_I, LoadTarget.A, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.INC, LoadTarget.HL, LoadTarget.HL, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.INC, LoadTarget.H, LoadTarget.H, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.DEC, LoadTarget.H, LoadTarget.H, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.H, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.DAA, LoadTarget.A, LoadTarget.A, 4, 1));
        this.instructionMap.push(new JP(JumpType.RELATIVE, JumpCondition.Z, LoadTarget.U8, 8, 2, 12, 3));
        this.instructionMap.push(new ALU(ALUOperation.ADD, LoadTarget.HL, LoadTarget.HL, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.M_HL_I, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.DEC, LoadTarget.HL, LoadTarget.HL, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.INC, LoadTarget.L, LoadTarget.L, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.DEC, LoadTarget.L, LoadTarget.L, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.L, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.CPL, LoadTarget.A, LoadTarget.A, 4, 1));

        // 0x30 - 0x3F
        this.instructionMap.push(new JP(JumpType.RELATIVE, JumpCondition.NC, LoadTarget.U8, 8, 2, 12, 3));
        this.instructionMap.push(new LD(LoadTarget.SP, LoadTarget.U16, 12, 3));
        this.instructionMap.push(new LD(LoadTarget.M_HL_D, LoadTarget.A, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.INC, LoadTarget.SP, LoadTarget.SP, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.INC, LoadTarget.M_HL, LoadTarget.M_HL, 12, 3));
        this.instructionMap.push(new ALU(ALUOperation.DEC, LoadTarget.M_HL, LoadTarget.M_HL, 12, 3));
        this.instructionMap.push(new LD(LoadTarget.M_HL, LoadTarget.U8, 12, 3));
        this.instructionMap.push(new ALU(ALUOperation.SCF, LoadTarget.A, LoadTarget.A, 4, 1));
        this.instructionMap.push(new JP(JumpType.RELATIVE, JumpCondition.C, LoadTarget.U8, 8, 2, 12, 3));
        this.instructionMap.push(new ALU(ALUOperation.ADD, LoadTarget.HL, LoadTarget.SP, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.M_HL_D, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.DEC, LoadTarget.SP, LoadTarget.SP, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.INC, LoadTarget.A, LoadTarget.A, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.DEC, LoadTarget.A, LoadTarget.A, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.CCF, LoadTarget.A, LoadTarget.A, 4, 1));

        // 0x40 - 0x4F
        this.instructionMap.push(new LD(LoadTarget.B, LoadTarget.B, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.B, LoadTarget.C, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.B, LoadTarget.D, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.B, LoadTarget.E, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.B, LoadTarget.H, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.B, LoadTarget.L, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.B, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.B, LoadTarget.A, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.C, LoadTarget.B, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.C, LoadTarget.C, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.C, LoadTarget.D, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.C, LoadTarget.E, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.C, LoadTarget.H, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.C, LoadTarget.L, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.C, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.C, LoadTarget.A, 4, 1));

        // 0x50 - 0x5F
        this.instructionMap.push(new LD(LoadTarget.D, LoadTarget.B, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.D, LoadTarget.C, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.D, LoadTarget.D, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.D, LoadTarget.E, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.D, LoadTarget.H, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.D, LoadTarget.L, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.D, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.D, LoadTarget.A, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.E, LoadTarget.B, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.E, LoadTarget.C, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.E, LoadTarget.D, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.E, LoadTarget.E, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.E, LoadTarget.H, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.E, LoadTarget.L, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.E, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.E, LoadTarget.A, 4, 1));

        // 0x60 - 0x6F
        this.instructionMap.push(new LD(LoadTarget.H, LoadTarget.B, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.H, LoadTarget.C, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.H, LoadTarget.D, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.H, LoadTarget.E, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.H, LoadTarget.H, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.H, LoadTarget.L, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.H, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.H, LoadTarget.A, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.L, LoadTarget.B, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.L, LoadTarget.C, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.L, LoadTarget.D, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.L, LoadTarget.E, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.L, LoadTarget.H, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.L, LoadTarget.L, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.L, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.L, LoadTarget.A, 4, 1));

        // 0x70 - 0x7F
        this.instructionMap.push(new LD(LoadTarget.M_HL, LoadTarget.B, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.M_HL, LoadTarget.C, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.M_HL, LoadTarget.D, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.M_HL, LoadTarget.E, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.M_HL, LoadTarget.H, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.M_HL, LoadTarget.L, 8, 2));
        this.instructionMap.push(new HALT(4, 1));
        this.instructionMap.push(new LD(LoadTarget.M_HL, LoadTarget.A, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.B, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.C, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.D, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.E, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.H, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.L, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.A, 4, 1));

        // 0x80 - 0x8F
        this.instructionMap.push(new ALU(ALUOperation.ADD, LoadTarget.A, LoadTarget.B, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.ADD, LoadTarget.A, LoadTarget.C, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.ADD, LoadTarget.A, LoadTarget.D, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.ADD, LoadTarget.A, LoadTarget.E, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.ADD, LoadTarget.A, LoadTarget.H, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.ADD, LoadTarget.A, LoadTarget.L, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.ADD, LoadTarget.A, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.ADD, LoadTarget.A, LoadTarget.A, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.ADC, LoadTarget.A, LoadTarget.B, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.ADC, LoadTarget.A, LoadTarget.C, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.ADC, LoadTarget.A, LoadTarget.D, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.ADC, LoadTarget.A, LoadTarget.E, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.ADC, LoadTarget.A, LoadTarget.H, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.ADC, LoadTarget.A, LoadTarget.L, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.ADC, LoadTarget.A, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.ADC, LoadTarget.A, LoadTarget.A, 4, 1));

        // 0x90 - 0x9F
        this.instructionMap.push(new ALU(ALUOperation.SUB, LoadTarget.A, LoadTarget.B, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SUB, LoadTarget.A, LoadTarget.C, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SUB, LoadTarget.A, LoadTarget.D, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SUB, LoadTarget.A, LoadTarget.E, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SUB, LoadTarget.A, LoadTarget.H, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SUB, LoadTarget.A, LoadTarget.L, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SUB, LoadTarget.A, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.SUB, LoadTarget.A, LoadTarget.A, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SBC, LoadTarget.A, LoadTarget.B, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SBC, LoadTarget.A, LoadTarget.C, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SBC, LoadTarget.A, LoadTarget.D, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SBC, LoadTarget.A, LoadTarget.E, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SBC, LoadTarget.A, LoadTarget.H, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SBC, LoadTarget.A, LoadTarget.L, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SBC, LoadTarget.A, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.SBC, LoadTarget.A, LoadTarget.A, 4, 1));

        // 0xA0 - 0xAF
        this.instructionMap.push(new ALU(ALUOperation.AND, LoadTarget.A, LoadTarget.B, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.AND, LoadTarget.A, LoadTarget.C, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.AND, LoadTarget.A, LoadTarget.D, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.AND, LoadTarget.A, LoadTarget.E, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.AND, LoadTarget.A, LoadTarget.H, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.AND, LoadTarget.A, LoadTarget.L, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.AND, LoadTarget.A, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.AND, LoadTarget.A, LoadTarget.A, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.XOR, LoadTarget.A, LoadTarget.B, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.XOR, LoadTarget.A, LoadTarget.C, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.XOR, LoadTarget.A, LoadTarget.D, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.XOR, LoadTarget.A, LoadTarget.E, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.XOR, LoadTarget.A, LoadTarget.H, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.XOR, LoadTarget.A, LoadTarget.L, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.XOR, LoadTarget.A, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.XOR, LoadTarget.A, LoadTarget.A, 4, 1));

        // 0xB0 - 0xBF
        this.instructionMap.push(new ALU(ALUOperation.OR, LoadTarget.A, LoadTarget.B, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.OR, LoadTarget.A, LoadTarget.C, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.OR, LoadTarget.A, LoadTarget.D, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.OR, LoadTarget.A, LoadTarget.E, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.OR, LoadTarget.A, LoadTarget.H, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.OR, LoadTarget.A, LoadTarget.L, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.OR, LoadTarget.A, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.OR, LoadTarget.A, LoadTarget.A, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.CP, LoadTarget.A, LoadTarget.B, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.CP, LoadTarget.A, LoadTarget.C, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.CP, LoadTarget.A, LoadTarget.D, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.CP, LoadTarget.A, LoadTarget.E, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.CP, LoadTarget.A, LoadTarget.H, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.CP, LoadTarget.A, LoadTarget.L, 4, 1));
        this.instructionMap.push(new ALU(ALUOperation.CP, LoadTarget.A, LoadTarget.M_HL, 8, 2));
        this.instructionMap.push(new ALU(ALUOperation.CP, LoadTarget.A, LoadTarget.A, 4, 1));

        // 0xC0 - 0xCF
        this.instructionMap.push(new JP(JumpType.RET, JumpCondition.NZ, LoadTarget.M_SP, 8, 2, 20, 5));
        this.instructionMap.push(new Stack(StackType.POP, LoadTarget.BC, LoadTarget.SP, 12, 3));
        this.instructionMap.push(new JP(JumpType.ABSOLUTE, JumpCondition.NZ, LoadTarget.U16, 12, 3, 16, 4));
        this.instructionMap.push(new JP(JumpType.ABSOLUTE, JumpCondition.ALWAYS, LoadTarget.U16, 16, 4, 16, 4));
        this.instructionMap.push(new JP(JumpType.CALL, JumpCondition.NZ, LoadTarget.U16, 12, 3, 24, 6));
        this.instructionMap.push(new Stack(StackType.PUSH, LoadTarget.SP, LoadTarget.BC, 16, 4));
        this.instructionMap.push(new ALU(ALUOperation.ADD, LoadTarget.A, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new JP(JumpType.RST_00, JumpCondition.ALWAYS, LoadTarget.A, 16, 4, 16, 4));
        this.instructionMap.push(new JP(JumpType.RET, JumpCondition.Z, LoadTarget.M_SP, 8, 2, 20, 5));
        this.instructionMap.push(new JP(JumpType.RET, JumpCondition.ALWAYS, LoadTarget.M_SP, 16, 4, 16, 4));
        this.instructionMap.push(new JP(JumpType.ABSOLUTE, JumpCondition.Z, LoadTarget.U16, 12, 3, 16, 4));
        this.instructionMap.push(null); // 0xCB
        this.instructionMap.push(new JP(JumpType.CALL, JumpCondition.Z, LoadTarget.U16, 12, 3, 24, 6));
        this.instructionMap.push(new JP(JumpType.CALL, JumpCondition.ALWAYS, LoadTarget.U16, 24, 6, 24, 6));
        this.instructionMap.push(new ALU(ALUOperation.ADC, LoadTarget.A, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new JP(JumpType.RST_08, JumpCondition.ALWAYS, LoadTarget.A, 16, 4, 16, 4));

        // 0xD0 - 0xDF
        this.instructionMap.push(new JP(JumpType.RET, JumpCondition.NC, LoadTarget.M_SP, 8, 2, 20, 5));
        this.instructionMap.push(new Stack(StackType.POP, LoadTarget.DE, LoadTarget.SP, 12, 3));
        this.instructionMap.push(new JP(JumpType.ABSOLUTE, JumpCondition.NC, LoadTarget.U16, 12, 3, 16, 4));
        this.instructionMap.push(new NOP(4, 1));
        this.instructionMap.push(new JP(JumpType.CALL, JumpCondition.NC, LoadTarget.U16, 12, 3, 24, 6));
        this.instructionMap.push(new Stack(StackType.PUSH, LoadTarget.SP, LoadTarget.DE, 16, 4));
        this.instructionMap.push(new ALU(ALUOperation.SUB, LoadTarget.A, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new JP(JumpType.RST_10, JumpCondition.ALWAYS, LoadTarget.A, 16, 4, 16, 4));
        this.instructionMap.push(new JP(JumpType.RET, JumpCondition.C, LoadTarget.M_SP, 8, 2, 20, 5));
        this.instructionMap.push(new JP(JumpType.RETI, JumpCondition.ALWAYS, LoadTarget.M_SP, 16, 4, 16, 4));
        this.instructionMap.push(new JP(JumpType.ABSOLUTE, JumpCondition.C, LoadTarget.U16, 12, 3, 16, 4));
        this.instructionMap.push(new NOP(4, 1));
        this.instructionMap.push(new JP(JumpType.CALL, JumpCondition.C, LoadTarget.U16, 12, 3, 24, 6));
        this.instructionMap.push(new NOP(4, 1));
        this.instructionMap.push(new ALU(ALUOperation.SBC, LoadTarget.A, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new JP(JumpType.RST_18, JumpCondition.ALWAYS, LoadTarget.A, 16, 4, 16, 4));

        // 0xE0 - 0xEF
        this.instructionMap.push(new LD(LoadTarget.FF00_U8, LoadTarget.A, 12, 3));
        this.instructionMap.push(new Stack(StackType.POP, LoadTarget.HL, LoadTarget.SP, 12, 3));
        this.instructionMap.push(new LD(LoadTarget.FF00_C, LoadTarget.A, 8, 2));
        this.instructionMap.push(new NOP(4, 1));
        this.instructionMap.push(new NOP(4, 1));
        this.instructionMap.push(new Stack(StackType.PUSH, LoadTarget.SP, LoadTarget.HL, 16, 4));
        this.instructionMap.push(new ALU(ALUOperation.AND, LoadTarget.A, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new JP(JumpType.RST_20, JumpCondition.ALWAYS, LoadTarget.A, 16, 4, 16, 4));
        this.instructionMap.push(new LD(LoadTarget.SP, LoadTarget.SP_E8, 16, 4));
        this.instructionMap.push(new JP(JumpType.ABSOLUTE, JumpCondition.ALWAYS, LoadTarget.HL, 4, 1, 4, 1));
        this.instructionMap.push(new LD(LoadTarget.M_U16, LoadTarget.A, 16, 4));
        this.instructionMap.push(new NOP(4, 1));
        this.instructionMap.push(new NOP(4, 1));
        this.instructionMap.push(new NOP(4, 1));
        this.instructionMap.push(new ALU(ALUOperation.XOR, LoadTarget.A, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new JP(JumpType.RST_28, JumpCondition.ALWAYS, LoadTarget.A, 16, 4, 16, 4));

        // 0xF0 - 0xFF
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.FF00_U8, 12, 3));
        this.instructionMap.push(new Stack(StackType.POP, LoadTarget.AF, LoadTarget.SP, 12, 3));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.FF00_C, 8, 2));
        this.instructionMap.push(new Interrupt(InterruptType.DISABLE, 4, 1));
        this.instructionMap.push(new NOP(4, 1));
        this.instructionMap.push(new Stack(StackType.PUSH, LoadTarget.SP, LoadTarget.AF, 16, 4));
        this.instructionMap.push(new ALU(ALUOperation.OR, LoadTarget.A, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new JP(JumpType.RST_30, JumpCondition.ALWAYS, LoadTarget.A, 16, 4, 16, 4));
        this.instructionMap.push(new LD(LoadTarget.HL, LoadTarget.SP_E8, 12, 3));
        this.instructionMap.push(new LD(LoadTarget.SP, LoadTarget.HL, 8, 2));
        this.instructionMap.push(new LD(LoadTarget.A, LoadTarget.M_U16, 16, 4));
        this.instructionMap.push(new Interrupt(InterruptType.ENABLE, 4, 1));
        this.instructionMap.push(new NOP(4, 1));
        this.instructionMap.push(new NOP(4, 1));
        this.instructionMap.push(new ALU(ALUOperation.CP, LoadTarget.A, LoadTarget.U8, 8, 2));
        this.instructionMap.push(new JP(JumpType.RST_38, JumpCondition.ALWAYS, LoadTarget.A, 16, 4, 16, 4));

        // CB opcodes are more predictable
        const cbTargets = [
            LoadTarget.B,
            LoadTarget.C,
            LoadTarget.D,
            LoadTarget.E,
            LoadTarget.H,
            LoadTarget.L,
            LoadTarget.M_HL,
            LoadTarget.A
        ];

        const cbOpTypes = [
            ALUOperation.RLC,
            ALUOperation.RRC,
            ALUOperation.RL,
            ALUOperation.RR,
            ALUOperation.SLA,
            ALUOperation.SRA,
            ALUOperation.SWAP,
            ALUOperation.SRL,
            ALUOperation.BIT0,
            ALUOperation.BIT1,
            ALUOperation.BIT2,
            ALUOperation.BIT3,
            ALUOperation.BIT4,
            ALUOperation.BIT5,
            ALUOperation.BIT6,
            ALUOperation.BIT7,
            ALUOperation.RES0,
            ALUOperation.RES1,
            ALUOperation.RES2,
            ALUOperation.RES3,
            ALUOperation.RES4,
            ALUOperation.RES5,
            ALUOperation.RES6,
            ALUOperation.RES7,
            ALUOperation.SET0,
            ALUOperation.SET1,
            ALUOperation.SET2,
            ALUOperation.SET3,
            ALUOperation.SET4,
            ALUOperation.SET5,
            ALUOperation.SET6,
            ALUOperation.SET7,
        ];

        for (let cbcode = 0; cbcode <= 0xff; cbcode++) {
            const upper = (cbcode & 0xf0) >> 4;
            const lower = cbcode & 0x0f;

            const target = cbTargets[lower % 8];
            const penalty16Bit = (lower === 6 || lower === 0xe) ? ((cbcode >= 0x40 && cbcode <= 0x7f) ? 1.5 : 2) : 1;
            const opType = cbOpTypes[(upper * 2) + (lower < 8 ? 0 : 1)];

            const instruction = new ALU(opType, target, target, 8 * penalty16Bit, 2 * penalty16Bit);

            this.instructionMapCB.push(instruction);
        }
    }

    public decode(opcode: number): Instruction {
        if (opcode === 0xcb) {
            this.registers.t += 4;
            opcode = this.memory.read(this.registers.pc++);
            return this.decodeCB(opcode);
        } else {
            return this.instructionMap[opcode];
        }
    }

    protected decodeCB(opcode: number): Instruction {
        const op = this.instructionMapCB[opcode];
        return op;
    }
}