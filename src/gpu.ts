import { Memory } from "memory";
import { Registers } from "registers";
import { Screen } from "screen";

enum GPUMode {
    HBLANK = 0,
    VBLANK = 1,
    OAM_READ = 2,
    VRAM_READ = 3
}

export class GPU {

    public vblank: boolean;

    private modeClock: number;

    private line: number;

    private mode: GPUMode;

    constructor(protected memory: Memory, protected registers: Registers, protected screen: Screen) {
        this.mode = GPUMode.OAM_READ;
        this.modeClock = 0;
        this.line = 0;
        this.vblank = false;
    }

    public tick(elapsed: number) {
        this.modeClock += elapsed;

        switch (this.mode) {
            case GPUMode.OAM_READ:
                if (this.modeClock >= 80) {
                    this.modeClock -= 80;
                    this.setMode(GPUMode.VRAM_READ);
                }
                break;
            case GPUMode.VRAM_READ:
                if (this.modeClock >= 172) {
                    this.modeClock -= 172;
                    this.setMode(GPUMode.HBLANK);

                    // Write a scanline to the framebuffer
                    this.screen.render();
                }
                break;
            case GPUMode.HBLANK:
                if (this.modeClock >= 204) {
                    this.modeClock -= 204;
                    this.nextLine();

                    if (this.line == 144) {
                        this.setMode(GPUMode.VBLANK);
                        this.screen.draw();
                        this.memory.write(Memory.IF, this.memory.read(Memory.IF) | 0x1);
                        this.registers.t = 0;
                        this.vblank = true;
                    } else {
                        this.setMode(GPUMode.OAM_READ);
                    }
                }
                break;
            case GPUMode.VBLANK:
                if (this.modeClock >= 456) {
                    this.modeClock -= 456;
                    this.nextLine();

                    if (this.line > 153) {
                        this.setMode(GPUMode.OAM_READ);
                        this.line = 0;
                        this.memory.write(Memory.LY, 0);
                    }
                    this.updateLY();
                }
                break;
        }
    }

    public nextLine() {
        this.line++;

        this.updateLY();
    }

    private updateLY() {
        this.memory.write(Memory.LY, this.line);

        let stat = this.memory.read(Memory.STAT);
        if (this.memory.read(Memory.LY) === this.memory.read(Memory.LYC)) {
            this.memory.write(Memory.STAT, stat | (1 << 2));
            if (stat & (1 << 6)) {
                this.memory.write(Memory.IF, this.memory.read(Memory.IF) | 0b10);
            }
        } else {
            this.memory.write(Memory.STAT, stat & (0xFF - (1 << 2)));
        }
    }

    private setMode(mode: GPUMode) {
        this.mode = mode;

        let stat = this.memory.read(Memory.STAT) & 0b11111100;
        stat |= this.mode
        this.memory.write(Memory.STAT, stat);

        if (this.mode < 3) {
            if (stat & (1 << (3 + this.mode))) {
                this.memory.write(Memory.IF, this.memory.read(Memory.IF) | 0b10);
            }
        }
    }
}