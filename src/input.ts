import { Memory } from "memory";

export class Input {
    private keys: { [key: number]: number } = {
        83: 0x80, // START
        65: 0x40, // SELECT
        88: 0x20, // B
        90: 0x10, // A
        40: 0x08, // DOWN
        38: 0x04, // UP
        37: 0x02, // LEFT
        39: 0x01 // RIGHT
    };

    private buttons: { [key: number]: number } = {
        9: 0x80, // START
        8: 0x40, // SELECT
        2: 0x20, // B
        0: 0x10, // A
        13: 0x08, // DOWN
        12: 0x04, // UP
        14: 0x02, // LEFT
        15: 0x01 // RIGHT
    };

    private state: number = 0;

    private gamepad: Gamepad;

    constructor(protected memory: Memory) {

        document.addEventListener("keydown", (e) => this.pressKey(e.keyCode));
        document.addEventListener("keyup", (e) => this.releaseKey(e.keyCode));

        window.addEventListener("gamepadconnected", (e: GamepadEvent) => {
            console.log("Gamepad connected");
            this.gamepad = e.gamepad;
            const update = setInterval(() => {
                this.gamepad = navigator.getGamepads()[0];

                // Gamepad
                if (this.gamepad) {
                    this.gamepad.buttons.forEach((button, buttonId) => {
                        let pressed = button.pressed;
                        const mask = this.buttons[buttonId];

                        const axes = this.gamepad.axes;
                        // Optional analogue sticks to override d-pad        
                        switch (buttonId) {
                            case 12:
                                if (axes[1] < -0.5) {
                                    pressed = true;
                                }
                                break;
                            case 13:
                                if (axes[1] > 0.5) {
                                    pressed = true;
                                }
                                break;
                            case 14:
                                if (axes[0] < -0.5) {
                                    pressed = true;
                                }
                                break;
                            case 15:
                                if (axes[0] > 0.5) {
                                    pressed = true;
                                }
                                break;
                        }

                        if (mask) {
                            if (pressed) {
                                if (!(this.state & mask)) {
                                    this.state |= mask;
                                    this.memory.write(Memory.IF, this.memory.read(Memory.IF) | 0xb100);
                                }
                            } else {
                                this.state &= (0xff - mask);
                            }
                        }
                    });
                }
            }, 0);
        });
    }

    public update() {
        var value = this.memory.read(Memory.P1);
        value = ((~value) & 0x30); // invert the value so 1 means 'active'
        if (value & 0x10) { // direction keys listened
            value |= (this.state & 0x0F);
        } else if (value & 0x20) { // action keys listened
            value |= ((this.state & 0xF0) >> 4);
        } else if ((value & 0x30) == 0) { // no keys listened
            value &= 0xF0;
        }

        value = ((~value) & 0x3F); // invert back
        this.memory.write(Memory.P1, value);
    };

    private pressKey(key: number) {
        this.state |= this.keys[key];

        this.memory.write(Memory.IF, this.memory.read(Memory.IF) | 0xb100);
    };

    private releaseKey(key: number) {
        var mask = 0xFF - this.keys[key];
        this.state &= mask;
    };
}