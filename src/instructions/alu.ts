import { Memory } from "memory";
import { Flags, Registers } from "registers";
import { Z80 } from "z80";
import { LD, LoadTarget } from "./ld";

export enum ALUOperation {
    ADD,
    INC,
    ADC,
    AND,
    SUB,
    CP,
    DEC,
    OR,
    SBC,
    XOR,
    CCF,
    SCF,
    CPL,
    RL,
    RLC,
    RR,
    RRC,
    RLA,
    RLCA,
    RRA,
    RRCA,
    SLA,
    SRA,
    SRL,
    SWAP,
    BIT0,
    BIT1,
    BIT2,
    BIT3,
    BIT4,
    BIT5,
    BIT6,
    BIT7,
    RES0,
    RES1,
    RES2,
    RES3,
    RES4,
    RES5,
    RES6,
    RES7,
    SET0,
    SET1,
    SET2,
    SET3,
    SET4,
    SET5,
    SET6,
    SET7,
    DAA
}

export class ALU extends LD {

    constructor(
        protected aluOp: ALUOperation,
        protected to: LoadTarget,
        protected from: LoadTarget,
        t: number,
        m: number
    ) {
        super(to, from, t, m);
    }

    protected calculate(registers: Registers, memory: Memory, current: number, value: number) {
        const overflow = this.is16BitTarget(this.to) ? 0xffff : 0xff;
        let result;

        switch (this.aluOp) {
            case ALUOperation.CPL:
                registers.setFlag(Flags.N, true);
                registers.setFlag(Flags.H, true);
                return (~value) & 0xff;
            case ALUOperation.CCF:
            case ALUOperation.SCF:
                registers.setFlag(Flags.N, false);
                registers.setFlag(Flags.H, false);
                registers.setFlag(Flags.C, (this.aluOp === ALUOperation.SCF) ? true : !registers.f_c);
                return current;
            case ALUOperation.ADC:
            case ALUOperation.ADD:
            case ALUOperation.INC:
                let a_carry = 0;
                if (this.aluOp === ALUOperation.INC) {
                    value = 1;
                }
                if (this.aluOp === ALUOperation.ADC) {
                    a_carry = registers.f_c;
                }
                result = (current + value + a_carry);

                // Zero
                if (!this.is16BitTarget(this.to)) {
                    registers.setFlag(Flags.Z, (result & overflow) === 0);
                }

                // Subtract flag
                if (!this.is16BitTarget(this.to) || (this.aluOp !== ALUOperation.INC)) {
                    registers.setFlag(Flags.N, false);
                }

                // Half carrys
                if (this.is16BitTarget(this.to)) {
                    if (this.aluOp !== ALUOperation.INC) {
                        registers.setFlag(Flags.H, !!(((current & 0xfff) + (value & 0xfff)) & 0x1000));
                    }
                } else {
                    registers.setFlag(Flags.H, ((current & 0xf) + (value & 0xf) + a_carry) > 0xf);
                }
                // Carry
                if (this.aluOp !== ALUOperation.INC) {
                    registers.setFlag(Flags.C, this.is16BitTarget(this.from) ? !!(result & 0x10000) : !!(result & 0x100));
                }
                return result & overflow;
            case ALUOperation.SUB:
            case ALUOperation.SBC:
            case ALUOperation.DEC:
            case ALUOperation.CP:
                let s_carry = 0;
                if (this.aluOp === ALUOperation.DEC) {
                    value = 1;
                }
                if (this.aluOp === ALUOperation.SBC) {
                    s_carry = registers.f_c;
                }
                result = (current - value) - s_carry;
                if (!this.is16BitTarget(this.to)) {
                    registers.setFlag(Flags.Z, (result & overflow) === 0);
                    registers.setFlag(Flags.N, true);
                    registers.setFlag(Flags.H, (current & 0xf) < ((value & 0xf) + s_carry));
                }
                if (this.aluOp !== ALUOperation.DEC) {
                    registers.setFlag(Flags.C, current < (value + s_carry));
                }
                if (this.aluOp === ALUOperation.CP) {
                    return current;
                }
                return result & overflow;
            case ALUOperation.AND:
                result = current & value;
                registers.setFlag(Flags.Z, result === 0);
                registers.setFlag(Flags.N, false);
                registers.setFlag(Flags.H, true);
                registers.setFlag(Flags.C, false);
                return result;
            case ALUOperation.OR:
            case ALUOperation.XOR:
                if (this.aluOp === ALUOperation.OR) {
                    result = current | value;
                } else {
                    result = current ^ value;
                }
                registers.setFlag(Flags.Z, result === 0);
                registers.setFlag(Flags.N, false);
                registers.setFlag(Flags.H, false);
                registers.setFlag(Flags.C, false);
                return result;
            case ALUOperation.RL:
            case ALUOperation.RLA:
                result = ((value << 1) + registers.f_c) & 0xff;
                if (this.aluOp === ALUOperation.RL) {
                    registers.setFlag(Flags.Z, result === 0);
                } else {
                    registers.setFlag(Flags.Z, false);
                }
                registers.setFlag(Flags.H, false);
                registers.setFlag(Flags.N, false);
                registers.setFlag(Flags.C, !!((value << 1) & 0x100));
                return result & overflow;
            case ALUOperation.RLC:
            case ALUOperation.RLCA:
                result = ((value << 1) + (((value << 1) & 0x100) ? 1 : 0)) & 0xff;
                if (this.aluOp === ALUOperation.RLC) {
                    registers.setFlag(Flags.Z, result === 0);
                } else {
                    registers.setFlag(Flags.Z, false);
                }
                registers.setFlag(Flags.H, false);
                registers.setFlag(Flags.N, false);
                registers.setFlag(Flags.C, !!((value << 1) & 0x100));
                return result & overflow;
            case ALUOperation.RR:
            case ALUOperation.RRA:
                result = (value >> 1) + (registers.f_c << 7);
                if (this.aluOp === ALUOperation.RR) {
                    registers.setFlag(Flags.Z, result === 0);
                } else {
                    registers.setFlag(Flags.Z, false);
                }
                registers.setFlag(Flags.H, false);
                registers.setFlag(Flags.N, false);
                registers.setFlag(Flags.C, !!(value & 0x1));
                return result & overflow;
            case ALUOperation.RRC:
            case ALUOperation.RRCA:
                result = (value >> 1) + ((value & 0x1) << 7);
                if (this.aluOp === ALUOperation.RRC) {
                    registers.setFlag(Flags.Z, result === 0);
                } else {
                    registers.setFlag(Flags.Z, false);
                }
                registers.setFlag(Flags.H, false);
                registers.setFlag(Flags.N, false);
                registers.setFlag(Flags.C, !!(value & 0x1));
                return result & overflow;
            case ALUOperation.SLA:
                result = (value << 1) & overflow;
                registers.setFlag(Flags.Z, result === 0);
                registers.setFlag(Flags.H, false);
                registers.setFlag(Flags.N, false);
                registers.setFlag(Flags.C, !!((value << 1) & 0x100));
                return result;
            case ALUOperation.SRA:
                result = (value >> 1) + (value & 0b10000000);
                registers.setFlag(Flags.Z, result === 0);
                registers.setFlag(Flags.H, false);
                registers.setFlag(Flags.N, false);
                registers.setFlag(Flags.C, !!(value & 0x1));
                return result & overflow;
            case ALUOperation.SRL:
                result = value >> 1;
                registers.setFlag(Flags.Z, result === 0);
                registers.setFlag(Flags.N, false);
                registers.setFlag(Flags.H, false);
                registers.setFlag(Flags.C, !!(value & 0x1));
                return result & overflow;
            case ALUOperation.SWAP:
                result = ((value & 0xf) << 4) + (value >> 4);
                registers.setFlag(Flags.Z, result === 0);
                registers.setFlag(Flags.N, false);
                registers.setFlag(Flags.H, false);
                registers.setFlag(Flags.C, false);
                return result;
            case ALUOperation.SET0:
            case ALUOperation.SET1:
            case ALUOperation.SET2:
            case ALUOperation.SET3:
            case ALUOperation.SET4:
            case ALUOperation.SET5:
            case ALUOperation.SET6:
            case ALUOperation.SET7:
                return value |= (1 << this.getTargetBit());
            case ALUOperation.RES0:
            case ALUOperation.RES1:
            case ALUOperation.RES2:
            case ALUOperation.RES3:
            case ALUOperation.RES4:
            case ALUOperation.RES5:
            case ALUOperation.RES6:
            case ALUOperation.RES7:
                return value &= ~(1 << this.getTargetBit());
            case ALUOperation.BIT0:
            case ALUOperation.BIT1:
            case ALUOperation.BIT2:
            case ALUOperation.BIT3:
            case ALUOperation.BIT4:
            case ALUOperation.BIT5:
            case ALUOperation.BIT6:
            case ALUOperation.BIT7:
                registers.setFlag(Flags.Z, !(current & (1 << this.getTargetBit())));
                registers.setFlag(Flags.N, false);
                registers.setFlag(Flags.H, true);
                return current;
            case ALUOperation.DAA:
                if (registers.f_n) {
                    if (registers.f_h) {
                        current = (current - 0x6) & 0xFF;
                    }
                    if (registers.f_c) {
                        current -= 0x60;
                    }
                } else {
                    if ((current & 0xF) > 9 || registers.f_h) {
                        current += 0x6;
                    }
                    if (current > 0x9F || registers.f_c) {
                        current += 0x60;
                    }
                }
                if (current & 0x100) {
                    registers.setFlag(Flags.C, true);
                }

                current &= 0xFF;
                registers.setFlag(Flags.H, false);
                registers.setFlag(Flags.Z, current === 0);

                return current;
        }
    }

    protected getTargetBit() {
        switch (this.aluOp) {
            case ALUOperation.BIT0:
            case ALUOperation.SET0:
            case ALUOperation.RES0:
                return 0;
            case ALUOperation.BIT1:
            case ALUOperation.SET1:
            case ALUOperation.RES1:
                return 1;
            case ALUOperation.BIT2:
            case ALUOperation.SET2:
            case ALUOperation.RES2:
                return 2;
            case ALUOperation.BIT3:
            case ALUOperation.SET3:
            case ALUOperation.RES3:
                return 3;
            case ALUOperation.BIT4:
            case ALUOperation.SET4:
            case ALUOperation.RES4:
                return 4;
            case ALUOperation.BIT5:
            case ALUOperation.SET5:
            case ALUOperation.RES5:
                return 5;
            case ALUOperation.BIT6:
            case ALUOperation.SET6:
            case ALUOperation.RES6:
                return 6;
            case ALUOperation.BIT7:
            case ALUOperation.SET7:
            case ALUOperation.RES7:
                return 7;
        }
    }
}