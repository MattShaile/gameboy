import { Memory } from "memory";
import { Registers } from "registers";
import { Instruction } from "./instruction";

export class HALT extends Instruction {
    public execute(
        memory: Memory,
        registers: Registers
    ) {
        registers.halt = true;
        super.execute(memory, registers);
    }
}