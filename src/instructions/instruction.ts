import { Memory } from "memory";
import { Registers } from "registers";
import { Z80 } from "z80";

export abstract class Instruction {
    constructor(
        protected t: number,
        protected m: number
    ) { }

    public execute(
        memory: Memory,
        registers: Registers
    ) {
        registers.t += this.t;
        registers.m += this.m;
    }
}