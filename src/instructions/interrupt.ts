import { Memory } from "memory";
import { Registers } from "registers";
import { Z80 } from "z80";
import { Instruction } from "./instruction";

export enum InterruptType {
    ENABLE,
    DISABLE
}

export class Interrupt extends Instruction {

    constructor(
        protected type: InterruptType,
        t: number,
        m: number
    ) {
        super(t, m);
    }

    public execute(
        memory: Memory,
        registers: Registers
    ) {
        registers.ime = (this.type === InterruptType.ENABLE) ? true : false;

        super.execute(memory, registers);
    }
}