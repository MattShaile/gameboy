import { Memory } from "memory";
import { Registers } from "registers";
import { LD, LoadTarget } from "./ld";

export enum JumpCondition {
    ALWAYS,
    Z,
    NZ,
    C,
    NC
}

export enum JumpType {
    ABSOLUTE,
    RELATIVE,
    CALL,
    RET,
    RETI,
    RST_00,
    RST_08,
    RST_10,
    RST_18,
    RST_20,
    RST_28,
    RST_30,
    RST_38,
    // No opcode, caused by interrupts
    RST_40,
    RST_48,
    RST_50,
    RST_58,
    RST_60,
}

export class JP extends LD {

    public static depth: number = 0;

    constructor(
        protected jumpType: JumpType,
        public jumpCondition: JumpCondition,
        protected from: LoadTarget,
        t: number,
        m: number,
        protected bt: number,
        protected bm: number
    ) {
        super(LoadTarget.PC, from, t, m);
    }

    protected calculate(registers: Registers, memory: Memory, current: number, value: number) {
        if (this.jumpCondition !== JumpCondition.ALWAYS) {
            switch (this.jumpCondition) {
                case JumpCondition.Z:
                    if (!registers.f_z) {
                        return current;
                    }
                    break;
                case JumpCondition.NZ:
                    if (registers.f_z) {
                        return current;
                    }
                    break;
                case JumpCondition.C:
                    if (!registers.f_c) {
                        return current;
                    }
                    break;
                case JumpCondition.NC:
                    if (registers.f_c) {
                        return current;
                    }
                    break;
            }
        }

        registers.t += this.bt - this.t;
        registers.m += this.bm - this.m;

        switch (this.jumpType) {
            case JumpType.ABSOLUTE:
                return value;
            case JumpType.RELATIVE:
                let offset = value;
                if (offset > 127) {
                    offset -= 256;
                }
                return current + offset;
            case JumpType.CALL:
            case JumpType.RST_00:
            case JumpType.RST_08:
            case JumpType.RST_10:
            case JumpType.RST_18:
            case JumpType.RST_20:
            case JumpType.RST_28:
            case JumpType.RST_30:
            case JumpType.RST_38:
            case JumpType.RST_40:
            case JumpType.RST_48:
            case JumpType.RST_50:
            case JumpType.RST_58:
            case JumpType.RST_60:
                JP.depth++;
                if (this.jumpType !== JumpType.CALL) {
                    value = this.decodeRST();
                }
                registers.sp -= 2;
                memory.write(registers.sp, registers.pc, 2);
                return value;
            case JumpType.RET:
            case JumpType.RETI:
                if (this.jumpType === JumpType.RETI) {
                    registers.ime = true;
                }
                JP.depth--;
                registers.sp += 2;
                return value;
        }
    }

    private decodeRST() {
        switch (this.jumpType) {
            case JumpType.RST_00:
                return 0x00;
            case JumpType.RST_08:
                return 0x08;
            case JumpType.RST_10:
                return 0x10;
            case JumpType.RST_18:
                return 0x18;
            case JumpType.RST_20:
                return 0x20;
            case JumpType.RST_28:
                return 0x28;
            case JumpType.RST_30:
                return 0x30;
            case JumpType.RST_38:
                return 0x38;
            case JumpType.RST_40:
                return 0x40;
            case JumpType.RST_48:
                return 0x48;
            case JumpType.RST_50:
                return 0x50;
            case JumpType.RST_58:
                return 0x58;
            case JumpType.RST_60:
                return 0x60;
        }
    }
}