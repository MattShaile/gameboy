import { Memory } from "memory";
import { Flags, Registers } from "registers";
import { Z80 } from "z80";
import { Instruction } from "./instruction";
export enum LoadTarget {
    // Registers
    A,
    B,
    C,
    D,
    E,
    AF,
    BC,
    DE,
    HL,
    H,
    L,
    SP,
    PC,
    // Values
    U8,
    U16,
    // Pointer
    M_U16,
    M_BC,
    M_DE,
    M_HL,
    M_HL_I,
    M_HL_D,
    M_SP,
    SP_E8,
    // Offsets
    FF00_U8,
    FF00_C
}

export class LD extends Instruction {

    constructor(
        protected to: LoadTarget,
        protected from: LoadTarget,
        t: number,
        m: number
    ) {
        super(t, m);
    }

    public execute(
        memory: Memory,
        registers: Registers
    ) {
        const value = this.read(memory, registers);
        this.write(memory, registers, value);

        registers.t += this.t;
        registers.m += this.m;
    }

    protected read(
        memory: Memory,
        registers: Registers
    ) {
        let lowerByte;
        let upperByte;

        switch (this.from) {
            case LoadTarget.A:
                return registers.a;
            case LoadTarget.B:
                return registers.b;
            case LoadTarget.C:
                return registers.c;
            case LoadTarget.D:
                return registers.d;
            case LoadTarget.E:
                return registers.e;
            case LoadTarget.AF:
                return registers.af;
            case LoadTarget.BC:
                return registers.bc;
            case LoadTarget.DE:
                return registers.de;
            case LoadTarget.HL:
                return registers.hl;
            case LoadTarget.H:
                return registers.h;
            case LoadTarget.L:
                return registers.l;
            case LoadTarget.SP:
                return registers.sp;
            case LoadTarget.PC:
                return registers.pc;
            case LoadTarget.U8:
                return memory.read(registers.pc++);
            case LoadTarget.U16:
                lowerByte = memory.read(registers.pc++);
                upperByte = memory.read(registers.pc++) << 8;
                return lowerByte + upperByte;
            case LoadTarget.M_U16:
                lowerByte = memory.read(registers.pc++);
                upperByte = memory.read(registers.pc++) << 8;
                return memory.read(lowerByte + upperByte);
            case LoadTarget.M_BC:
                return memory.read(registers.bc);
            case LoadTarget.M_DE:
                return memory.read(registers.de);
            case LoadTarget.M_HL:
                return memory.read(registers.hl);
            case LoadTarget.M_HL_I:
                return memory.read(registers.hl++);
            case LoadTarget.M_HL_D:
                return memory.read(registers.hl--);
            case LoadTarget.M_SP:
                return memory.read(registers.sp) + (memory.read(registers.sp + 1) << 8);
            case LoadTarget.SP_E8:
                let offset = memory.read(registers.pc++);
                if (offset > 127) {
                    offset = offset - 256;
                }
                const result = registers.sp + offset;
                registers.setFlag(Flags.Z, false);
                registers.setFlag(Flags.N, false);
                registers.setFlag(Flags.H, ((registers.sp & 0xf) + (offset & 0xf)) > 0xf);
                registers.setFlag(Flags.C, ((registers.sp & 0xff) + (offset & 0xff)) > 0xff);
                return result & 0xffff;
            case LoadTarget.FF00_U8:
                return memory.read((memory.read(registers.pc++) + 0xff00) & 0xffff);
            case LoadTarget.FF00_C:
                return memory.read((registers.c + 0xff00) & 0xffff);
        }
    }

    protected write(
        memory: Memory,
        registers: Registers,
        value: number
    ) {
        let current;

        let bytesToWrite = this.is16BitTarget(this.from) ? 2 : 1;

        switch (this.to) {
            case LoadTarget.A:
                return registers.a = this.calculate(registers, memory, registers.a, value);
            case LoadTarget.B:
                return registers.b = this.calculate(registers, memory, registers.b, value);
            case LoadTarget.C:
                return registers.c = this.calculate(registers, memory, registers.c, value);
            case LoadTarget.D:
                return registers.d = this.calculate(registers, memory, registers.d, value);
            case LoadTarget.E:
                return registers.e = this.calculate(registers, memory, registers.e, value);
            case LoadTarget.AF:
                return registers.af = this.calculate(registers, memory, registers.af, value);
            case LoadTarget.BC:
                return registers.bc = this.calculate(registers, memory, registers.bc, value);
            case LoadTarget.DE:
                return registers.de = this.calculate(registers, memory, registers.de, value);
            case LoadTarget.HL:
                return registers.hl = this.calculate(registers, memory, registers.hl, value);
            case LoadTarget.H:
                return registers.h = this.calculate(registers, memory, registers.h, value);
            case LoadTarget.L:
                return registers.l = this.calculate(registers, memory, registers.l, value);
            case LoadTarget.SP:
                return registers.sp = this.calculate(registers, memory, registers.sp, value);
            case LoadTarget.PC:
                return registers.pc = this.calculate(registers, memory, registers.pc, value);
            case LoadTarget.M_U16:
                current = memory.read(registers.pc++) + (memory.read(registers.pc++) << 8);
                return memory.write(current, this.calculate(registers, memory, current, value), bytesToWrite);
            case LoadTarget.M_BC:
                current = memory.read(registers.bc);
                return memory.write(registers.bc, this.calculate(registers, memory, current, value), bytesToWrite);
            case LoadTarget.M_DE:
                current = memory.read(registers.de);
                return memory.write(registers.de, this.calculate(registers, memory, current, value), bytesToWrite);
            case LoadTarget.M_HL:
                current = memory.read(registers.hl);
                return memory.write(registers.hl, this.calculate(registers, memory, current, value), bytesToWrite);
            case LoadTarget.M_HL_I:
                return memory.write(registers.hl++, value, bytesToWrite);
            case LoadTarget.M_HL_D:
                return memory.write(registers.hl--, value, bytesToWrite);
            case LoadTarget.FF00_U8:
                current = memory.read(registers.pc++) + 0xff00;
                return memory.write(current, this.calculate(registers, memory, current, value), bytesToWrite);
            case LoadTarget.FF00_C:
                current = registers.c + 0xff00;
                return memory.write(current, this.calculate(registers, memory, current, value), bytesToWrite);
        }
    }

    protected calculate(registers: Registers, memory: Memory, current: number, value: number) {
        return value;
    }

    protected is16BitTarget(value: LoadTarget) {
        return value === LoadTarget.AF ||
            value === LoadTarget.BC ||
            value === LoadTarget.DE ||
            value === LoadTarget.HL ||
            value === LoadTarget.SP ||
            value === LoadTarget.PC ||
            value === LoadTarget.U16
    }
}
