import { Memory } from "memory";
import { Registers } from "registers";
import { Z80 } from "z80";
import { Instruction } from "./instruction";

export class NOP extends Instruction {
}