import { Memory } from "memory";
import { Registers } from "registers";
import { Z80 } from "z80";
import { Instruction } from "./instruction";
import { LD, LoadTarget } from "./ld";
export enum StackType {
    PUSH,
    POP
}

export class Stack extends LD {

    constructor(
        protected type: StackType,
        protected to: LoadTarget,
        protected from: LoadTarget,
        t: number,
        m: number
    ) {
        super(to, from, t, m);
    }

    public execute(
        memory: Memory,
        registers: Registers
    ) {
        switch (this.type) {
            case StackType.PUSH:
                registers.sp -= 2;
                const read = this.read(memory, registers);
                memory.write(registers.sp, read, 2);
                break;
            case StackType.POP:
                const a = memory.read(registers.sp);
                const b = memory.read(registers.sp + 1) << 8;
                const value = a + b;

                registers.sp += 2;
                this.write(memory, registers, value);
                break;
        }

        registers.t += this.t;
        registers.m += this.m;
    }
}
