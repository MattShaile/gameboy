import { Memory } from "memory";
import { Registers } from "registers";
import { Instruction } from "./instruction";

export class STOP extends Instruction {
    public execute(
        memory: Memory,
        registers: Registers
    ) {
        registers.pc++;
        super.execute(memory, registers);
    }
}