import { Decoder } from "decoder";
import { GPU } from "gpu";
import { Memory } from "memory";
import { Registers } from "registers";

export class MapViewer {

    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;

    private renderBuffer: ImageData;

    private colors: number[] = [
        0x9bbc0f,
        0x8bac0f,
        0x306230,
        0x0f380f
    ]

    constructor(
        protected memory: Memory,
        protected registers: Registers
    ) {
        this.canvas = document.getElementById("background") as HTMLCanvasElement;
        this.context = this.canvas.getContext("2d");

        this.renderBuffer = this.context.createImageData(256, 256);
        this.canvas.width = 256;
        this.canvas.height = 256;

        for (var i = 0; i < 256 * 256 * 4; i += 4) {
            const color = this.colors[0];
            this.renderBuffer.data[i] = color >> 16 & 0xff;
            this.renderBuffer.data[i + 1] = color >> 8 & 0xff;
            this.renderBuffer.data[i + 2] = color & 0xff;
            this.renderBuffer.data[i + 3] = 0xff;
        }
    }

    public render() {

    }

    public draw() {

    }

    public drawBG() {
        const lcdc = this.memory.read(Memory.LCDC);
        const tileMap2 = !!(lcdc & 8);
        const tileSet2 = !(lcdc & 16);

        // Palette
        const paletteByte = this.memory.read(Memory.BG_PALETTE);
        const palette = [
            paletteByte & 0b11,
            (paletteByte >> 2) & 0b11,
            (paletteByte >> 4) & 0b11,
            (paletteByte >> 6) & 0b11
        ];

        // TODO: Select correct bg map
        const mapAddress = tileMap2 ? Memory.TILE_MAP_2 : Memory.TILE_MAP_1;
        const tilesetAddress = tileSet2 ? Memory.TILE_SET_2 : Memory.TILE_SET_1;

        for (let ty = 0; ty < 32; ty++) {
            for (let tx = 0; tx < 32; tx++) {
                let tileIndex = this.memory.read(mapAddress + tx + ty * 32);
                if (tileSet2) {
                    if (tileIndex > 127) {
                        tileIndex -= 256;
                    }
                    tileIndex += 128;
                }
                const tileAddressOfsset = tileIndex * 16;
                for (let y = 0; y < 8; y++) {
                    const tileAddress = tilesetAddress + tileAddressOfsset + y * 2;
                    const lowerByte = this.memory.read(tileAddress);
                    const upperByte = this.memory.read(tileAddress + 1);

                    for (let x = 0; x < 8; x++) {
                        const bitMask = 0b10000000 >> x;
                        let paletteIndex = 0;
                        if (lowerByte & bitMask) {
                            paletteIndex += 1;
                        }
                        if (upperByte & bitMask) {
                            paletteIndex += 2;
                        }
                        const colorIndex = palette[paletteIndex];
                        const color = this.colors[colorIndex];

                        this.drawPixel(tx + ty * 32, x, y, color);
                    }
                }
            }
        }
        const scx = this.memory.read(Memory.SCX);
        const scy = this.memory.read(Memory.SCY);
        for (let x = 0; x < 160; x++) {
            for (let y = 0; y < 144; y++) {
                if (x === 0 || x === 159 || y === 0 || y === 143) {
                    let screenY = y + scy;
                    this.drawPixel(-1, x + scx, (y + scy) % 256, 0xff0000);
                }
            }
        }

        this.context.putImageData(this.renderBuffer, 0, 0);
    }

    private drawPixel(tileIndex: number, x: number, y: number, color: number) {
        let absX = x;
        let absY = y;
        if (tileIndex > -1) {
            absX = (tileIndex % 32) * 8 + x;
            absY = Math.floor(tileIndex / 32) * 8 + y;
        }
        const n = (absX * 4) + (absY * 256 * 4);

        this.renderBuffer.data[n] = color >> 16 & 0xff;
        this.renderBuffer.data[n + 1] = color >> 8 & 0xff;
        this.renderBuffer.data[n + 2] = color & 0xff;
        this.renderBuffer.data[n + 3] = 0xff;
    }
}