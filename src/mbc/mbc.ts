export interface MBC {
    read(address: number): number;
    write(address: number, value: number, bytes: number): void;
}