import { Memory } from "memory";
import { MBC } from "./mbc";

export class MBC0 implements MBC {

    private memory: number[];

    constructor(rom: number[]) {
        this.memory = [...rom];
    }

    public write(address: number, value: number, bytes: number = 1) {
        if (address < 0x8000) {
            return;
        }

        // DMA transfer
        if (address === 0xFF46) {
            const startAddress = (value << 8);
            for (var i = 0; i < 0xA0; i++) {
                this.memory[Memory.OAM + i] = this.memory[startAddress + i];
            }
            return;
        }

        if (bytes > 1) {
            this.memory[address] = value & 0xff;
            this.memory[address + 1] = (value & 0xff00) >> 8;
        } else {
            this.memory[address] = value;
        }
    }

    public read(address: number): number {
        let byte;
        if (!this.memory[Memory.DISABLE_BIOS] && address < 0x100) {
            byte = Memory.BIOS[address];
        } else {
            byte = this.memory[address] || 0;
        }

        return byte;
    }
}