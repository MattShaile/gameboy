import { Memory } from "memory";
import { MBC } from "./mbc";

enum Mode {
    ROM,
    RAM
}

export class MBC1 implements MBC {

    public static ROM_SIZE: number = 0x148;
    public static RAM_SIZE: number = 0x149;

    protected banks: number;
    protected romSize: number;
    protected ramSize: number;

    protected rom: number[];
    protected memory: number[];

    protected ramEnable: boolean;
    protected ramBank: number;
    protected romBank: number;
    protected mode: Mode;

    protected externalRAM: number[][];

    constructor(rom: number[]) {
        this.rom = rom;
        this.memory = [];

        const romSizeData = this.memory[MBC1.ROM_SIZE];

        this.romSize = 0x8000 << romSizeData;
        this.banks = this.romSize > 0 ? (this.romSize / 0x4000) : 0;
        this.ramSize = [0, 1, 2, 4][this.memory[MBC1.RAM_SIZE]] << 10;

        this.ramEnable = false;
        this.mode = Mode.ROM;
        this.ramBank = 0;
        this.romBank = 1;

        this.externalRAM = [[], [], [], []];
    }

    public write(address: number, value: number, bytes: number = 1) {

        // DMA transfer
        if (address === 0xFF46) {
            const startAddress = (value << 8);
            for (var i = 0; i < 0xA0; i++) {
                this.memory[Memory.OAM + i] = this.memory[startAddress + i];
            }
            return;
        }

        // RAM enable
        if (address >= 0x0 && address <= 0x1fff) {
            this.ramEnable = !!(value & 0x0a);
            return;
        }

        // Lower Bits of ROM Bank Number
        if (address >= 0x2000 && address <= 0x3fff) {
            if (this.mode === Mode.ROM) {
                this.romBank = ((this.romBank & 0b11100000) + (value & 0b11111));
            }
            return;
        }

        // RAM Bank Number OR Upper Bits of ROM Bank Number
        if (address >= 0x4000 && address <= 0x5fff) {
            if (this.mode === Mode.RAM) {
                this.ramBank = value;
            } else {
                this.romBank = (value << 5) + (value & 0b11111);
            }
            return;
        }

        // ROM/RAM Mode
        if (address >= 0x6000 && address <= 0x7fff) {
            this.mode = (value & 1 ? Mode.RAM : Mode.ROM);
            return;
        }

        // External RAM
        if (address >= 0xa000 && address <= 0xbfff) {
            const externalRAMBank = this.externalRAM[this.ramBank];
            if (bytes > 1) {
                externalRAMBank[address - 0xa000] = value & 0xff;
                externalRAMBank[address - 0xa000 + 1] = (value & 0xff00) >> 8;
            } else {
                externalRAMBank[address - 0xa000] = value;
            }
            return;
        }

        if (bytes > 1) {
            this.memory[address] = value & 0xff;
            this.memory[address + 1] = (value & 0xff00) >> 8;
        } else {
            this.memory[address] = value;
        }
    }

    public read(address: number): number {
        // Bios
        if (!this.memory[Memory.DISABLE_BIOS] && address < 0x100) {
            return Memory.BIOS[address] || 0;
        }

        // External RAM
        if (address >= 0xa000 && address <= 0xbfff) {
            const externalRAMBank = this.externalRAM[this.ramBank];

            return externalRAMBank[address - 0xa000] || 0;
        }

        // ROM Bank
        if (address >= 0x4000 && address <= 0x7fff) {
            let bank = this.romBank;

            if (bank === 0 || bank === 0x20 || bank === 0x40 || bank === 0x60) {
                bank++;
            }

            return this.rom[(bank * 0x4000) + (address & 0x3fff)] || 0;
        }

        if (address <= 0x3fff) {
            return this.rom[address] || 0;
        }

        return this.memory[address] || 0;
    }
}