import { Memory } from "memory";
import { MBC } from "./mbc";
import { MBC1 } from "./mbc1";

enum Mode {
    ROM,
    RAM
}

export class MBC3 extends MBC1 {

    protected rtcEnabled: boolean;
    protected rtcSelect: number;
    protected rtcRegisters: number[];
    protected rtcRegistersLatched: number[];
    protected rtcLatchLast: number;

    // TODO: Finish implementing RTC

    constructor(rom: number[]) {
        super(rom);

        this.rtcEnabled = false;
        this.rtcSelect = 0;
        this.rtcRegisters = [];
        this.rtcRegistersLatched = [];

        this.rtcLatchLast = 2;
    }

    public write(address: number, value: number, bytes: number = 1) {
        if (address >= 0x0 && address <= 0x1fff) {
            this.ramEnable = !!(value & 0x0a);
            return;
        }

        if (address >= 0x2000 && address <= 0x3fff) {
            this.romBank = (value & 0b1111111);
            return;
        }

        if (address >= 0x4000 && address <= 0x5fff) {
            if (this.ramEnable) {
                if (value >= 0x8 && value <= 0xc) {
                    this.rtcEnabled = true;
                    this.rtcSelect = value;
                } else {
                    this.rtcEnabled = false;
                    this.ramBank = value;
                }
            }
            return;
        }

        if (address >= 0xa000 && address <= 0xbfff) {
            if (this.ramEnable) {
                if (this.rtcEnabled) {
                    this.rtcRegisters[this.rtcSelect - 8] = value;
                } else {
                    const externalRAMBank = this.externalRAM[this.ramBank];
                    if (bytes > 1) {
                        externalRAMBank[address - 0xa000] = value & 0xff;
                        externalRAMBank[address - 0xa000 + 1] = (value & 0xff00) >> 8;
                    } else {
                        externalRAMBank[address - 0xa000] = value;
                    }
                }
            }
            return;
        }

        if (address >= 0x6000 && address <= 0x7fff) {
            if (this.rtcLatchLast === 0 && value === 1) {
                this.rtcRegistersLatched = [...this.rtcRegisters];
            }
            this.rtcLatchLast = value;

            return;
        }

        super.write(address, value);
    }

    public read(address: number): number {
        // ROM Bank
        if (address >= 0x4000 && address <= 0x7fff) {
            let bank = this.romBank;

            if (bank === 0) {
                bank++;
            }

            return this.rom[(bank * 0x4000) + (address & 0x3fff)] || 0;
        }

        return super.read(address);
    }
}