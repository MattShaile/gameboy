import { Memory } from "memory";

export enum Flags {
    Z = 0b10000000,
    N = 0b01000000,
    H = 0b00100000,
    C = 0b00010000
}

export class Registers {
    public a: number;
    public b: number;
    public c: number;
    public d: number;
    public e: number;

    public h: number;
    public l: number;

    public f: number;

    public pc: number;

    public sp: number;

    public ime: boolean = false;
    public halt: boolean = false;

    public m: number;
    public t: number;

    private html: HTMLElement;

    constructor(protected memory: Memory) {
        this.reset();

        this.html = document.getElementById("registers");
    }

    public reset() {
        this.a = 0;
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = 0;

        this.h = 0;
        this.l = 0;

        this.f = 0;

        this.pc = 0;

        this.sp = 65534;

        this.m = 0;
        this.t = 0;
    }

    public get af() {
        return (this.a << 8) + this.f;
    }

    public get bc() {
        return (this.b << 8) + this.c;
    }

    public get de() {
        return (this.d << 8) + this.e;
    }

    public get hl() {
        return (this.h << 8) + this.l;
    }

    public set af(value: number) {
        this.a = value >> 8;
        this.f = value & 0xf0; // Only first 4 bits should ever be set
    }

    public set bc(value: number) {
        this.b = value >> 8;
        this.c = value & 0xff;
    }

    public set de(value: number) {
        this.d = value >> 8;
        this.e = value & 0xff;
    }

    public set hl(value: number) {
        this.h = value >> 8;
        this.l = value & 0xff;
    }

    public get f_z() {
        return (this.f & 0b10000000) ? 1 : 0;
    }
    public get f_n() {
        return (this.f & 0b01000000) ? 1 : 0;
    }
    public get f_h() {
        return (this.f & 0b00100000) ? 1 : 0;
    }
    public get f_c() {
        return (this.f & 0b00010000) ? 1 : 0;
    }

    public setFlag(flag: Flags, value: boolean) {
        if (value) {
            this.f |= flag;
        } else {
            this.f &= ~flag;
        }
    }

    public draw() {
        this.html.innerHTML = `
<table>
<tr>
<td>A</td>
<td>F</td>
</tr>
<tr>
<td>${this.formatAsHex(this.a)}</td>
<td>${this.formatAsHex(this.f)}</td>
</tr>
<tr>
<td>B</td>
<td>C</td>
</tr>
<tr>
<td>${this.formatAsHex(this.b)}</td>
<td>${this.formatAsHex(this.c)}</td>
</tr>
<tr>
<td>D</td>
<td>E</td>
</tr>
<tr>
<td>${this.formatAsHex(this.d)}</td>
<td>${this.formatAsHex(this.e)}</td>
</tr>
<tr>
<td>H</td>
<td>L</td>
</tr>
<tr>
<td>${this.formatAsHex(this.h)}</td>
<td>${this.formatAsHex(this.l)}</td>
</tr>
<tr>
<td colspan="2">PC</td>
</tr>
<tr>
<td colspan="2">${this.formatAsHex(this.pc, 4)}</td>
</tr>
<tr>
<td colspan="2">SP</td>
</tr>
<tr>
<td colspan="2">${this.formatAsHex(this.sp, 4)}</td>
</tr>
</table>
`;
    }

    private formatAsHex(n: number, digits: number = 2) {
        let hex = n.toString(16);

        while (hex.length < digits) {
            hex = "0" + hex;
        }

        return "0x" + hex;
    }
}