import { Decoder } from "decoder";
import { GPU } from "gpu";
import { Memory } from "memory";
import { Registers } from "registers";

export class Screen {

    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;

    private renderBuffer: ImageData;
    private bgTransparency: boolean[];

    // Green (classic)
    // private colors: number[] = [
    //     0x9bbc0f,
    //     0x8bac0f,
    //     0x306230,
    //     0x0f380f
    // ]

    // White
    private colors: number[] = [
        0xffffff,
        0xaaaaaa,
        0x555555,
        0x111111
    ]

    constructor(
        protected memory: Memory,
        protected registers: Registers
    ) {
        this.canvas = document.getElementById("screen") as HTMLCanvasElement;
        this.context = this.canvas.getContext("2d");
        // this.context.imageSmoothingEnabled = false;

        this.renderBuffer = this.context.createImageData(160, 144);
        this.canvas.width = 160;
        this.canvas.height = 144;

        for (var i = 0; i < 160 * 144 * 4; i += 4) {
            const color = this.colors[0];
            this.renderBuffer.data[i] = color >> 16 & 0xff;
            this.renderBuffer.data[i + 1] = color >> 8 & 0xff;
            this.renderBuffer.data[i + 2] = color & 0xff;
            this.renderBuffer.data[i + 3] = 0xff;
        }

        this.bgTransparency = [];
    }

    public render() {
        const lcdc = this.memory.read(Memory.LCDC);
        const bgEnabled = !!(lcdc & 1);
        const spritesEnabled = !!(lcdc & 2);
        const tallSprites = !!(lcdc & 4);
        const tileMap = !!(lcdc & 8);
        const tileSet = !(lcdc & 16);
        const windowEnabled = !!(lcdc & 32);
        const windowMap = !!(lcdc & 64);
        const disableLCD = !(lcdc & 128);

        if (disableLCD) {
            return;
        }

        // Palette
        const bgPaletteByte = this.memory.read(Memory.BG_PALETTE);
        const bgPalette = this.readPalette(bgPaletteByte);

        const tilemapAddress = tileMap ? Memory.TILE_MAP_2 : Memory.TILE_MAP_1;
        const windowmapAddress = windowMap ? Memory.TILE_MAP_2 : Memory.TILE_MAP_1;
        const tilesetAddress = tileSet ? Memory.TILE_SET_2 : Memory.TILE_SET_1;

        // Scroll x and y
        const scx = this.memory.read(Memory.SCX);
        const scy = this.memory.read(Memory.SCY);

        // Window x and y
        const wx = this.memory.read(Memory.WX) - 7;
        const wy = this.memory.read(Memory.WY);

        const screenLine = this.memory.read(Memory.LY);

        let bgLine = (screenLine + scy) % 256;
        const mapTY = Math.floor(bgLine / 8);
        const mapY = bgLine - (mapTY * 8);

        let windowLine = (screenLine - wy);
        const windowTY = Math.floor(windowLine / 8);
        const windowY = windowLine - (windowTY * 8);

        // Render BG
        if (bgEnabled) {
            for (let tx = 0; tx < 32; tx++) {
                let tileIndex = this.memory.read(tilemapAddress + tx + mapTY * 32);
                if (tileSet) {
                    if (tileIndex > 127) {
                        tileIndex -= 256;
                    }
                    tileIndex += 128;
                }
                const tileAddressOfsset = tileIndex * 16;
                const tileAddress = tilesetAddress + tileAddressOfsset + mapY * 2;
                const lowerByte = this.memory.read(tileAddress);
                const upperByte = this.memory.read(tileAddress + 1);

                for (let x = 0; x < 8; x++) {
                    let mapX = (tx * 8 + x);
                    if (scx + 160 > 255) {
                        if (mapX < scx && mapX > scx - 95) {
                            continue;
                        }
                    } else {
                        if (mapX < scx || mapX > scx + 160) {
                            continue;
                        }
                    }

                    let screenX = mapX - scx;
                    if (screenX < 0) {
                        screenX += 255;
                    }

                    const bitMask = 0b10000000 >> x;
                    let paletteIndex = 0;
                    if (lowerByte & bitMask) {
                        paletteIndex += 1;
                    }
                    if (upperByte & bitMask) {
                        paletteIndex += 2;
                    }
                    const colorIndex = bgPalette[paletteIndex];
                    const color = this.colors[colorIndex];

                    if (paletteIndex === 0) {
                        this.bgTransparency[screenX + screenLine * 160] = true;
                    } else {
                        this.bgTransparency[screenX + screenLine * 160] = false;
                    }
                    this.drawPixel(screenX, screenLine, color);
                }
            }
        }

        // Render windows
        if (windowEnabled) {
            for (let tx = 0; tx < 32; tx++) {
                let tileIndex = this.memory.read(windowmapAddress + tx + windowTY * 32);
                if (tileSet) {
                    if (tileIndex > 127) {
                        tileIndex -= 256;
                    }
                    tileIndex += 128;
                }
                const tileAddressOfsset = tileIndex * 16;
                const tileAddress = tilesetAddress + tileAddressOfsset + windowY * 2;
                const lowerByte = this.memory.read(tileAddress);
                const upperByte = this.memory.read(tileAddress + 1);

                for (let x = 0; x < 8; x++) {
                    let mapX = (tx * 8 + x);
                    if (mapX + wx < 0 || mapX + wx >= 160) {
                        continue;
                    }
                    if (screenLine - wy < 0 || screenLine - wy > 144) {
                        continue;
                    }

                    let screenX = mapX;

                    const bitMask = 0b10000000 >> x;
                    let paletteIndex = 0;
                    if (lowerByte & bitMask) {
                        paletteIndex += 1;
                    }
                    if (upperByte & bitMask) {
                        paletteIndex += 2;
                    }
                    const colorIndex = bgPalette[paletteIndex];
                    const color = this.colors[colorIndex];

                    if (paletteIndex > 0) {
                        this.bgTransparency[screenX + screenLine * 160] = false;
                    }
                    this.drawPixel(screenX, screenLine, color);
                }
            }
        }

        // Render Sprites
        if (spritesEnabled) {
            for (let spriteIndex = 39; spriteIndex >= 0; spriteIndex--) {
                const oamAddress = Memory.OAM + spriteIndex * 4;
                const spriteY = this.memory.read(oamAddress) - 16;
                const spriteX = this.memory.read(oamAddress + 1) - 8;
                // 8x8 tile index
                const spriteTileIndex = this.memory.read(oamAddress + 2);
                // 8x16 tile indexes
                const spriteTileIndexUpper = spriteTileIndex | 0x1;
                const spriteTileIndexLower = spriteTileIndex & 0xfe;
                const spriteInfo = this.memory.read(oamAddress + 3);

                const behind = !!(spriteInfo & 0b10000000);
                const flipY = !!(spriteInfo & 0b1000000);
                const flipX = !!(spriteInfo & 0b100000);
                const spritePalette = (spriteInfo & 0b10000) ? this.readPalette(this.memory.read(Memory.OBJ_PALETTE_2)) : this.readPalette(this.memory.read(Memory.OBJ_PALETTE_1));

                // TODO: implement 10 sprite per line limit
                for (let y = 0; y < (tallSprites ? 16 : 8); y++) {
                    if (spriteY + y === screenLine) {
                        let tileAddressOffset = spriteTileIndex * 16;
                        // if (!tallSprites) {
                        //     if (y < 8) {
                        //         tileAddressOffset = spriteTileIndexUpper * 16;
                        //     } else {
                        //         tileAddressOffset = spriteTileIndexLower * 16;
                        //     }
                        // }
                        let tileAddress = Memory.TILE_SET_1 + tileAddressOffset;
                        if (flipY) {
                            tileAddress += 14 - (y * 2);
                        } else {
                            tileAddress += y * 2;
                        }
                        const lowerByte = this.memory.read(tileAddress);
                        const upperByte = this.memory.read(tileAddress + 1);

                        for (let x = 7; x >= 0; x--) {
                            const spriteSX = spriteX + x;
                            if (spriteSX >= 0 && spriteSX < 160) {
                                let pixelMask;
                                if (flipX) {
                                    pixelMask = 1 << x;
                                } else {
                                    pixelMask = 0b10000000 >> x;
                                }
                                let paletteIndex = 0;
                                if (lowerByte & pixelMask) {
                                    paletteIndex += 1;
                                }
                                if (upperByte & pixelMask) {
                                    paletteIndex += 2;
                                }
                                const colorIndex = spritePalette[paletteIndex];
                                const color = this.colors[colorIndex];

                                // Transparency
                                if (paletteIndex > 0 && color) {
                                    this.drawPixel(spriteSX, screenLine, color, behind);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public draw() {
        this.context.putImageData(this.renderBuffer, 0, 0);
    }

    private drawPixel(x: number, y: number, color: number, behind: boolean = false) {
        const n = (x * 4) + (y * 160 * 4);

        if (behind) {
            if (!this.bgTransparency[x + y * 160]) {
                return;
            }
        }

        this.renderBuffer.data[n] = color >> 16 & 0xff;
        this.renderBuffer.data[n + 1] = color >> 8 & 0xff;
        this.renderBuffer.data[n + 2] = color & 0xff;
        this.renderBuffer.data[n + 3] = 0xff;
    }

    private readPalette(paletteByte: number) {
        return [
            paletteByte & 0b11,
            (paletteByte >> 2) & 0b11,
            (paletteByte >> 4) & 0b11,
            (paletteByte >> 6) & 0b11
        ];
    }
}