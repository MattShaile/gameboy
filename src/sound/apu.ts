import { Memory } from "memory";
import { Registers } from "registers";
import { Channel0 } from "./channel0";

export class APU {
    private audioContext: AudioContext;

    private channel0: Channel0;

    constructor(protected memory: Memory, protected registers: Registers) {
        document.addEventListener("click", () => this.start());
    }

    private start() {
        this.audioContext = new (window.AudioContext || (window as any).webkitAudioContext)();

        this.channel0 = new Channel0(this.audioContext);
    }
}