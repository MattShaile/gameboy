import * as chai from "chai";
import { expect } from "chai";
import { Decoder } from "decoder";
import { GPU } from "gpu";
import { JP, JumpCondition } from "instructions/jp";
import { Memory } from "memory";
import "mocha";
import { Flags, Registers } from "registers";
import * as sinonChai from "sinon-chai";
import { Z80 } from "z80";

chai.use(sinonChai);

const opReference = require("../opinfo.json");

const memory = new Memory();
// Turn off bios
memory.write(Memory.DISABLE_BIOS, 1);
const registers = new Registers(memory);
const z80 = new Z80(memory, registers);

describe("ALU: ", () => {

    it(`ADD`, () => {
        forAllCombinations8(
            "[A] + [B] = [C]",
            (a, b) => {
                program([
                    0x3e, a, // LD A,u8
                    0x06, b, // LD B,u8
                    0x80 // ADD A,B
                ]);

                run();

                return registers.a;
            },
            (a, b) => {
                return (a + b) & 0xff;
            },
            (a, b) => {
                return (a + b) > 0xff;
            }
        )
    });

    it(`ADC (with carry)`, () => {
        forAllCombinations8(
            "[A] + [B] = [C]",
            (a, b) => {
                registers.setFlag(Flags.C, true);

                program([
                    0x3e, a, // LD A,u8
                    0x06, b, // LD B,u8
                    0x88 // ADC A,B
                ]);

                run();

                return registers.a;
            },
            (a, b) => {
                return (a + b + 1) & 0xff;
            },
            (a, b) => {
                return (a + b + 1) > 0xff;
            }
        )
    });

    it(`ADC (without carry)`, () => {
        forAllCombinations8(
            "[A] + [B] = [C]",
            (a, b) => {
                registers.setFlag(Flags.C, false);

                program([
                    0x3e, a, // LD A,u8
                    0x06, b, // LD B,u8
                    0x88 // ADC A,B
                ]);

                run();

                return registers.a;
            },
            (a, b) => {
                return (a + b) & 0xff;
            },
            (a, b) => {
                return (a + b) > 0xff;
            }
        )
    });

    it(`SUB`, () => {
        forAllCombinations8(
            "[A] - [B] = [C]",
            (a, b) => {
                program([
                    0x3e, a, // LD A,u8
                    0x06, b, // LD B,u8
                    0x90 // SUB A,B
                ]);

                run();

                return registers.a;
            },
            (a, b) => {
                return (a - b) & 0xff;
            },
            (a, b) => {
                return b > a;
            }
        )
    });

    it(`SBC (with carry)`, () => {
        forAllCombinations8(
            "[A] - [B] = [C]",
            (a, b) => {
                registers.setFlag(Flags.C, true);

                program([
                    0x3e, a, // LD A,u8
                    0x06, b, // LD B,u8
                    0x98 // SBC A,B
                ]);

                run();

                return registers.a;
            },
            (a, b) => {
                return (a - (b + 1)) & 0xff;
            },
            (a, b) => {
                return (b + 1) > a;
            }
        )
    });

    it(`SBC (without carry)`, () => {
        forAllCombinations8(
            "[A] - [B] = [C]",
            (a, b) => {
                registers.setFlag(Flags.C, false);

                program([
                    0x3e, a, // LD A,u8
                    0x06, b, // LD B,u8
                    0x98 // SBC A,B
                ]);

                run();

                return registers.a;
            },
            (a, b) => {
                return (a - b) & 0xff;
            },
            (a, b) => {
                return b > a;
            }
        )
    });

    it(`AND`, () => {
        forAllCombinations8(
            "[A] & [B] = [C]",
            (a, b) => {
                program([
                    0x3e, a, // LD A,u8
                    0x06, b, // LD B,u8
                    0xa0 // AND A,B
                ]);

                run();

                return registers.a;
            },
            (a, b) => {
                return (a & b) & 0xff;
            },
            (a, b) => {
                return false;
            }
        )
    });

    it(`OR`, () => {
        forAllCombinations8(
            "[A] | [B] = [C]",
            (a, b) => {
                program([
                    0x3e, a, // LD A,u8
                    0x06, b, // LD B,u8
                    0xb0 // OR A,B
                ]);

                run();

                return registers.a;
            },
            (a, b) => {
                return (a | b) & 0xff;
            },
            (a, b) => {
                return false;
            }
        )
    });

    it(`XOR`, () => {
        forAllCombinations8(
            "[A] ^ [B] = [C]",
            (a, b) => {
                program([
                    0x3e, a, // LD A,u8
                    0x06, b, // LD B,u8
                    0xa8 // XOR A,B
                ]);

                run();

                return registers.a;
            },
            (a, b) => {
                return (a ^ b) & 0xff;
            },
            (a, b) => {
                return false;
            }
        )
    });

    it(`CP`, () => {
        forAllCombinations8(
            "Compare, A should be unchanged, [A], [B]",
            (a, b) => {
                program([
                    0x3e, a, // LD A,u8
                    0x06, b, // LD B,u8
                    0xb8 // CP A,B
                ]);

                run();

                return registers.a;
            },
            (a, b) => {
                return a;
            },
            (a, b) => {
                return b > a;
            },
            (a, b) => {
                return (b - a) === 0;
            }
        )
    });

    it(`INC`, () => {
        forAllCombinations8(
            "[A]++ [C]",
            (a, b) => {
                program([
                    0x3e, a, // LD A,u8
                    0x3c // INC A
                ]);

                run();

                return registers.a;
            },
            (a, b) => {
                return (a + 1) & 0xff;
            },
            () => {
                return false;
            }
        )
    });

    it(`DEC`, () => {
        forAllCombinations8(
            "[A]-- [C]",
            (a, b) => {
                program([
                    0x3e, a, // LD A,u8
                    0x3d // DEC A
                ]);

                run();

                return registers.a;
            },
            (a, b) => {
                return (a - 1) & 0xff;
            },
            (a) => {
                return false;
            }
        )
    });

    // Takes a loooong time to run, so don't run every time
    it.skip(`ADD 16`, () => {
        forAllCombinations16(
            "[HL] + [BC] = [R]",
            (hl, bc) => {
                const h = (hl >> 8) & 0xff;
                const l = hl & 0xff;
                const b = (bc >> 8) & 0xff;
                const c = bc & 0xff;

                program([
                    0x21, l, h, // LD HL,u16
                    0x06, b, // LD B,u8
                    0x0e, c, // LD C,u8
                    0x09 // ADD HL,BC
                ]);

                run();

                return registers.hl;
            },
            (hl, bc) => {
                return (hl + bc) & 0xffff;
            }
        )
    });

    it(`INC u16`, () => {
        forAllCombinationsHL16(
            "[HL] ++ = [R]",
            (hl) => {
                const h = (hl >> 8) & 0xff;
                const l = hl & 0xff;

                program([
                    0x21, l, h, // LD HL,u16
                    0x23 // INC HL
                ]);

                run();

                return registers.hl;
            },
            (hl) => {
                return (hl + 1) & 0xffff;
            }
        )
    });

    it(`DEC u16`, () => {
        forAllCombinationsHL16(
            "[HL] -- = [R]",
            (hl) => {
                const h = (hl >> 8) & 0xff;
                const l = hl & 0xff;

                program([
                    0x21, l, h, // LD HL,u16
                    0x2b // INC HL
                ]);

                run();

                return registers.hl;
            },
            (hl) => {
                return (hl - 1) & 0xffff;
            }
        )
    });

    it("ADD SP,u8", () => {
        const spInitialValue = 200;

        for (let i = 0; i < 0xff; i++) {
            registers.reset();

            registers.sp = spInitialValue;

            program([
                0xE8, i // ADD SP,i8
            ]);

            run();

            let answer = spInitialValue;
            if (i <= 127) {
                answer += i;
            } else {
                answer += (i - 256);
            }

            expect(registers.sp, `ADD SP,0x${i.toString(16)}`).to.equal(answer);
        }
    });
});

const forAllCombinations8 = (
    description: string,
    test: (a: number, b: number) => number,
    answer: (a: number, b: number) => number,
    carry: (a: number, b: number) => boolean,
    zero: (a: number, b: number) => boolean = null,
) => {
    for (let a = 0; a <= 0xff; a++) {
        for (let b = 0; b <= 0xff; b++) {
            registers.reset();

            const result = test(a, b);
            const expected = answer(a, b);

            const testDescription = description.replace(/\[A\]/g, a.toString()).replace(/\[B\]/g, b.toString()).replace(/\[C\]/g, expected.toString());

            expect(result, testDescription).to.equal(expected);

            // Check flags
            expect(!!registers.f_c, "(carry) " + testDescription).to.equal(carry(a, b));
            expect(!!registers.f_z, "(zero) " + testDescription).to.equal(zero ? zero(a, b) : result === 0);
        }
    }
}

const forAllCombinations16 = (
    description: string,
    test: (hl: number, bc: number) => number,
    answer: (hl: number, bc: number) => number
) => {
    for (let hl = 0; hl <= 0xffff; hl++) {
        for (let bc = 0; bc <= 0xffff; bc++) {
            registers.reset();

            const result = test(hl, bc);
            const expected = answer(hl, bc);

            const testDescription = description.replace(/\[HL\]/g, hl.toString()).replace(/\[BC\]/g, bc.toString()).replace(/\[R\]/g, expected.toString());

            expect(result, testDescription).to.equal(expected);
        }
    }
}


const forAllCombinationsHL16 = (
    description: string,
    test: (hl: number) => number,
    answer: (hl: number) => number
) => {
    for (let hl = 0; hl <= 0xffff; hl++) {
        registers.reset();

        const result = test(hl);
        const expected = answer(hl);

        const testDescription = description.replace(/\[HL\]/g, hl.toString()).replace(/\[R\]/g, expected.toString());

        expect(result, testDescription).to.equal(expected);
    }
}

const program = (opcodes: number[]) => {
    // Clear big chunk of memory at the start
    for (let i = 0; i < 100; i++) {
        memory.write(i, 0);
    }
    opcodes.forEach((opcode, index) => {
        memory.write(index, opcode);
    });
}

const run = () => {
    while (memory.read(registers.pc) > 0) {
        z80.tick();
    }
}
