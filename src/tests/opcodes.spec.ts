import * as chai from "chai";
import { expect } from "chai";
import { Decoder } from "decoder";
import { GPU } from "gpu";
import { JP, JumpCondition } from "instructions/jp";
import { Memory } from "memory";
import "mocha";
import { Flags, Registers } from "registers";
import * as sinonChai from "sinon-chai";
import { Z80 } from "z80";

chai.use(sinonChai);

const opReference = require("../opinfo.json");

const memory = new Memory();
// Turn off bios
memory.write(Memory.DISABLE_BIOS, 1);
const registers = new Registers(memory);
const z80 = new Z80(memory, registers);
const decoder = new Decoder(z80, memory, registers);

describe("Op code overview", () => {
    it("should all be supported", () => {

        let supportedCodes = 0;

        for (let i = 0; i <= 0xff; i++) {
            if (i === 0xcb) {
                for (let cb = 0; cb <= 0xff; cb++) {
                    memory.write(1, cb);
                    const instructionCB = decoder.decode(i);
                    if (instructionCB) {
                        supportedCodes++;
                    }
                }
            } else {
                const instruction = decoder.decode(i);
                if (instruction) {
                    supportedCodes++;
                }
            }
        }

        expect(supportedCodes).to.equal(0xff * 2 + 1);
    });
});

describe("Op code timings + fixed flags", () => {
    for (let opcode = 0; opcode <= 0xff; opcode++) {

        for (let cbcode = 0; cbcode <= (opcode === 0xcb ? 0xff : 0); cbcode++) {

            const opinfo = (opcode === 0xcb ? opReference.CBPrefixed[cbcode] : opReference.Unprefixed[opcode]);

            if (opinfo.Name === "UNUSED") {
                continue;
            }

            let opcodeId;
            if (opcode === 0xcb) {
                opcodeId = `(0xcb) 0x${cbcode.toString(16)} ${opinfo.Name}`;
            } else {
                opcodeId = `0x${opcode.toString(16)} ${opinfo.Name}`;
            }

            it(`${opcodeId}: should set t and m correctly`, () => {
                registers.reset();

                let t = opinfo.TCyclesNoBranch;
                let tb = opinfo.TCyclesBranch;

                registers.pc = 0x8000;
                memory.write(0x8000, cbcode);
                const instruction = decoder.decode(opcode);

                if (instruction) {
                    if (instruction instanceof JP && instruction.jumpCondition !== JumpCondition.ALWAYS) {
                        // Check both branches of a jump
                        registers.setFlag(Flags.Z, true);
                        registers.setFlag(Flags.C, true);
                        instruction.execute(memory, registers);
                        if (instruction.jumpCondition === JumpCondition.Z || instruction.jumpCondition === JumpCondition.C) {
                            expect(registers.t).to.eq(tb);
                            expect(registers.m).to.eq(tb / 4);
                        } else {
                            expect(registers.t).to.eq(t);
                            expect(registers.m).to.eq(t / 4);
                        }

                        registers.reset();

                        registers.setFlag(Flags.Z, false);
                        registers.setFlag(Flags.C, false);
                        instruction.execute(memory, registers);
                        if (instruction.jumpCondition === JumpCondition.Z || instruction.jumpCondition === JumpCondition.C) {
                            expect(registers.t).to.eq(t);
                            expect(registers.m).to.eq(t / 4);
                        } else {
                            expect(registers.t).to.eq(tb);
                            expect(registers.m).to.eq(tb / 4);
                        }
                    } else {
                        instruction.execute(memory, registers);
                        expect(registers.t).to.equal(t);
                        expect(registers.m).to.equal(t / 4);
                    }
                } else {
                    // throw new Error("Missing op");
                }
            });

            it(`${opcodeId}: should set pc correctly`, () => {
                registers.reset();

                const p = 0x7fff + opinfo.Length;

                registers.pc = 0x8000;
                memory.write(0x8000, cbcode);
                const instruction = decoder.decode(opcode);
                if (instruction) {
                    // Never jump so we get an accurate pc count
                    if (instruction instanceof JP && instruction.jumpCondition !== JumpCondition.ALWAYS) {
                        // Check both branches of a jump
                        registers.setFlag(Flags.Z, instruction.jumpCondition !== JumpCondition.Z);
                        registers.setFlag(Flags.C, instruction.jumpCondition !== JumpCondition.C);
                    } else if (instruction instanceof JP) {
                        // Skip test for immediate jumps, too much of a pain
                        return;
                    }

                    instruction.execute(memory, registers);

                    expect(registers.pc).to.equal(p);
                } else {
                    // throw new Error("Missing op");
                }
            });

            it(`${opcodeId}: should set Z flag correctly`, () => {
                registers.reset();

                const flags = opinfo.Flags;

                registers.pc = 0x8000;
                memory.write(0x8000, cbcode);
                const instruction = decoder.decode(opcode);
                if (instruction) {
                    instruction.execute(memory, registers);

                    if (flags.Z === "0") {
                        registers.setFlag(Flags.Z, true);
                        instruction.execute(memory, registers);
                        expect(registers.f_z).to.equal(0);
                    }
                    if (flags.Z === "1") {
                        registers.setFlag(Flags.Z, false);
                        instruction.execute(memory, registers);
                        expect(registers.f_z).to.equal(1);
                    }
                    if (flags.Z === "-") {
                        registers.setFlag(Flags.Z, true);
                        instruction.execute(memory, registers);
                        expect(registers.f_z).to.equal(1);
                        registers.setFlag(Flags.Z, false);
                        instruction.execute(memory, registers);
                        expect(registers.f_z).to.equal(0);
                    }
                } else {
                    // throw new Error("Missing op");
                }
            });

            it(`${opcodeId}: should set S flag correctly`, () => {
                registers.reset();

                const flags = opinfo.Flags;

                registers.pc = 0x8000;
                memory.write(0x8000, cbcode);
                const instruction = decoder.decode(opcode);
                if (instruction) {
                    instruction.execute(memory, registers);

                    if (flags.N === "0") {
                        registers.setFlag(Flags.N, true);
                        instruction.execute(memory, registers);
                        expect(registers.f_n).to.equal(0);
                    }
                    if (flags.N === "1") {
                        registers.setFlag(Flags.N, false);
                        instruction.execute(memory, registers);
                        expect(registers.f_n).to.equal(1);
                    }
                    if (flags.N === "-") {
                        registers.setFlag(Flags.N, true);
                        instruction.execute(memory, registers);
                        expect(registers.f_n).to.equal(1);
                        registers.setFlag(Flags.N, false);
                        instruction.execute(memory, registers);
                        expect(registers.f_n).to.equal(0);
                    }
                } else {
                    // throw new Error("Missing op");
                }
            });

            it(`${opcodeId}: should set H flag correctly`, () => {
                registers.reset();

                const flags = opinfo.Flags;

                registers.pc = 0x8000;
                memory.write(0x8000, cbcode);
                const instruction = decoder.decode(opcode);
                if (instruction) {
                    instruction.execute(memory, registers);

                    if (flags.H === "0") {
                        registers.setFlag(Flags.H, true);
                        instruction.execute(memory, registers);
                        expect(registers.f_h).to.equal(0);
                    }
                    if (flags.H === "1") {
                        registers.setFlag(Flags.H, false);
                        instruction.execute(memory, registers);
                        expect(registers.f_h).to.equal(1);
                    }
                    if (flags.H === "-") {
                        registers.setFlag(Flags.H, true);
                        instruction.execute(memory, registers);
                        expect(registers.f_h).to.equal(1);
                        registers.setFlag(Flags.H, false);
                        instruction.execute(memory, registers);
                        expect(registers.f_h).to.equal(0);
                    }
                } else {
                    // throw new Error("Missing op");
                }
            });

            it(`${opcodeId}: should set C flag correctly`, () => {
                registers.reset();

                const flags = opinfo.Flags;

                registers.pc = 0x8000;
                memory.write(0x8000, cbcode);
                const instruction = decoder.decode(opcode);
                if (instruction) {
                    instruction.execute(memory, registers);

                    if (flags.C === "0") {
                        registers.setFlag(Flags.C, true);
                        instruction.execute(memory, registers);
                        expect(registers.f_c).to.equal(0);
                    }
                    if (flags.C === "1") {
                        registers.setFlag(Flags.C, false);
                        instruction.execute(memory, registers);
                        expect(registers.f_c).to.equal(1);
                    }
                    if (flags.C === "-") {
                        registers.setFlag(Flags.C, true);
                        instruction.execute(memory, registers);
                        expect(registers.f_c).to.equal(1);
                        registers.setFlag(Flags.C, false);
                        instruction.execute(memory, registers);
                        expect(registers.f_c).to.equal(0);
                    }
                } else {
                    // throw new Error("Missing op");
                }
            });
        }
    }
});

