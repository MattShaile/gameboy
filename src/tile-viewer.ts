import { Decoder } from "decoder";
import { GPU } from "gpu";
import { Memory } from "memory";
import { Registers } from "registers";

export class TileViewer {

    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;

    private renderBuffer: ImageData;

    private colors: number[] = [
        0x9bbc0f,
        0x8bac0f,
        0x306230,
        0x0f380f
    ]

    constructor(
        protected memory: Memory,
        protected registers: Registers
    ) {
        this.canvas = document.getElementById("tiles") as HTMLCanvasElement;
        this.context = this.canvas.getContext("2d");

        this.renderBuffer = this.context.createImageData(128, 192);
        this.canvas.width = 128;
        this.canvas.height = 192;

        for (var i = 0; i < 128 * 192 * 4; i += 4) {
            const color = this.colors[3];
            this.renderBuffer.data[i] = color >> 16 & 0xff;
            this.renderBuffer.data[i + 1] = color >> 8 & 0xff;
            this.renderBuffer.data[i + 2] = color & 0xff;
            this.renderBuffer.data[i + 3] = 0xff;
        }

        this.context.putImageData(this.renderBuffer, 0, 0);
    }

    public draw() {
        const paletteByte = this.memory.read(Memory.BG_PALETTE);
        const palette = [
            paletteByte & 0b11,
            (paletteByte >> 2) & 0b11,
            (paletteByte >> 4) & 0b11,
            (paletteByte >> 6) & 0b11
        ];

        for (let address = Memory.TILE_SET_1; address <= 0x97FF; address += 2) {
            const addressOffset = address & 0x1fff;
            const tileIndex = Math.floor(addressOffset / 16);

            const y = (addressOffset >> 1) & 7;

            const lowerByte = this.memory.read(address);
            const upperByte = this.memory.read(address + 1);

            for (let x = 0; x < 8; x++) {
                const bitMask = 0b10000000 >> x;
                let paletteIndex = 0;
                if (lowerByte & bitMask) {
                    paletteIndex += 1;
                }
                if (upperByte & bitMask) {
                    paletteIndex += 2;
                }
                const colorIndex = palette[paletteIndex];
                const color = this.colors[colorIndex];

                this.drawPixel(tileIndex, x, y, color);
            }
        }

        for (let x = 0; x < 128; x++) {
            for (let y = 0; y < 128; y++) {
                if (x === 0 || x === 127 || y === 0 || y === 127) {
                    this.drawPixel(-1, x, y, 0xff0000);
                }
            }
        }
        for (let x = 0; x < 128; x++) {
            for (let y = 64; y < 192; y++) {
                if (x === 0 || x === 127 || y === 64 || y === 191) {
                    this.drawPixel(-1, x, y, 0x0000ff);
                }
            }
        }

        this.context.putImageData(this.renderBuffer, 0, 0);
    }

    private drawPixel(tileIndex: number, x: number, y: number, color: number) {
        let absX = x;
        let absY = y;
        if (tileIndex > -1) {
            absX = (tileIndex % 16) * 8 + x;
            absY = Math.floor(tileIndex / 16) * 8 + y;
        }

        let alpha = 0xff;
        if ((absX % 8) === 0 || (absY % 8) === 0) {
            alpha = 0xff / 2;
        }

        const n = (absX * 4) + (absY * 128 * 4);

        this.renderBuffer.data[n] = color >> 16 & 0xff;
        this.renderBuffer.data[n + 1] = color >> 8 & 0xff;
        this.renderBuffer.data[n + 2] = color & 0xff;
        this.renderBuffer.data[n + 3] = alpha;
    }
}