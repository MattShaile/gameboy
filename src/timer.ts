import { Memory } from "memory";
import { Registers } from "registers";

export class Timer {

    private clock: number;
    private divClock: number;

    constructor(
        protected memory: Memory,
        protected registers: Registers
    ) {
        memory.write(Memory.DIV, 0);
        memory.write(Memory.TIMA, 0);
        memory.write(Memory.TMA, 0);
        memory.write(Memory.TAC, 0);

        this.clock = 0;
        this.divClock = 0;
    }

    public tick(dt: number) {
        this.updateDiv(dt);
        this.updateTimer(dt);
    }

    private updateDiv(dt: number) {
        this.divClock += dt;
        if (this.divClock > 256) {
            this.divClock -= 256;
            const div = this.memory.read(Memory.DIV) + 1;
            this.memory.write(Memory.DIV, div & 0xff);
        }
    }

    private updateTimer(dt: number) {
        if (!(this.memory.read(Memory.TAC) & 0x4)) {
            return;
        }
        this.clock += dt;

        let threshold = 64;
        switch (this.memory.read(Memory.TAC) & 0x3) {
            case 0: threshold = 64; break; // 4KHz
            case 1: threshold = 1; break; // 256KHz
            case 2: threshold = 4; break; // 64KHz
            case 3: threshold = 16; break; // 16KHz
        }
        threshold *= 16;

        while (this.clock >= threshold) {
            this.clock -= threshold;

            const newTIMA = this.memory.read(Memory.TIMA) + 1;

            if (newTIMA > 0xff) {
                this.memory.write(Memory.TIMA, this.memory.read(Memory.TMA));

                this.memory.write(Memory.IF, this.memory.read(Memory.IF) | 0x4);
            } else {
                this.memory.write(Memory.TIMA, newTIMA & 0xff);
            }
        }
    }
}