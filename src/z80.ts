import { Decoder } from "decoder";
import { GPU } from "gpu";
import { Input } from "input";
import { JP, JumpCondition, JumpType } from "instructions/jp";
import { LoadTarget } from "instructions/ld";
import { Memory } from "memory";
import { Registers } from "registers";
import { Timer } from "timer";

const opReference = require("opinfo.json");
export class Z80 {

    public static debug: boolean;

    private interruptInstructions = [
        new JP(JumpType.RST_40, JumpCondition.ALWAYS, LoadTarget.A, 16, 4, 16, 4),
        new JP(JumpType.RST_48, JumpCondition.ALWAYS, LoadTarget.A, 16, 4, 16, 4),
        new JP(JumpType.RST_50, JumpCondition.ALWAYS, LoadTarget.A, 16, 4, 16, 4),
        new JP(JumpType.RST_58, JumpCondition.ALWAYS, LoadTarget.A, 16, 4, 16, 4),
        new JP(JumpType.RST_60, JumpCondition.ALWAYS, LoadTarget.A, 16, 4, 16, 4)
    ];

    private decoder: Decoder;

    constructor(
        protected memory: Memory,
        protected registers: Registers,
        protected gpu?: GPU,
        protected timer?: Timer,
        protected input?: Input
    ) {
        this.decoder = new Decoder(this, memory, registers);
    }

    public tick() {
        const w = (window as any);
        const pc = this.registers.pc;
        const a = this.registers.a;
        const f = this.registers.f;
        const t = this.registers.t;
        let opcode: number;

        if (this.registers.halt) {
            this.registers.t += 4;
            this.registers.m++;
        } else {
            opcode = this.memory.read(this.registers.pc++);

            this.registers.pc &= 0xffff;

            let instruction = this.decoder.decode(opcode);

            if (!instruction) {
                console.log("missing instruction:", this.debugString(pc, opcode));
            }
            if (instruction) {
                instruction.execute(this.memory, this.registers);
            }
            if (Z80.debug) {
                console.log(this.debugString(pc, opcode), f, "=>", this.registers.f, a, "=>", this.registers.a);
            }
        }

        const elapsed = this.registers.t - t;
        this.gpu?.tick(elapsed);
        this.timer?.tick(elapsed);
        this.input.update();

        // Handle interrupts
        let interruptValues = this.memory.read(Memory.IF);
        const interruptEnabled = this.memory.read(Memory.IE);
        const interruptPending = (interruptValues & interruptEnabled) > 0;
        if ((this.registers.halt && interruptPending)) {
            this.registers.halt = false;
        }
        if (this.registers.ime) {
            for (var i = 0; i < 5; i++) {
                const bit = 1 << i;
                if ((interruptValues & bit) && (interruptEnabled & bit)) {
                    interruptValues &= ~bit;
                    this.memory.write(Memory.IF, interruptValues);
                    this.registers.ime = false;
                    this.registers.t += 4;
                    this.interruptInstructions[i].execute(this.memory, this.registers);
                    break;
                }
            }
        }
    }

    private debugString(pc: number, opcode: number) {
        const ref = opReference.Unprefixed[opcode];

        let tabs = "";
        for (let i = 0; i < JP.depth; i++) {
            tabs += "\t";
        }

        if (JP.depth < 0) {
            throw new Error("Call stack problem detected");
        }

        let out = tabs + "0x" + pc.toString(16) + " 0x" + opcode.toString(16) + " " + ref.Name;
        if (ref.Length > 1) {
            out += "[";
            out += "0x" + this.memory.read(pc + 1).toString(16);
            if (ref.Length > 2) {
                out += ", 0x" + this.memory.read(pc + 2).toString(16);
            }
            out += "]";
        }

        return out;
    }
}